package br.com.systemmcr.sysfinan.testapi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.systemmcr.sysfinan.model.Despesa;
import br.com.systemmcr.sysfinan.repository.DespesaRepository;
import br.com.systemmcr.sysfinan.util.DatabaseCleaner;
import br.com.systemmcr.sysfinan.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class ApiDespesaIT {
	
	private static final int DESPESA_ID_INEXISTENTE = 100;
	
	private int quantidadeDespesas;
	
	private String jsonDespesas_Despesa03_Create;
	private String jsonDespesas_Despesa01_Update;
	
	private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzeXNmaW5hbi5zeXN0ZW1tY3JAZ21haWwuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5vbWUiOiJBZG1pbmlzdHJhZG9yIiwiZXhwIjoxNTc0OTY1NTA5LCJhdXRob3JpdGllcyI6WyJST0xFX0NPTlRBUEFHQVJfRklMVFJBUiIsIlJPTEVfVVNVQVJJT19GSUxUUkFSIiwiUk9MRV9VU1VBUklPX0NSSUFSIiwiUk9MRV9VU1VBUklPX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQ1JJQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9DUklBUiIsIlJPTEVfUkVDRUlUQV9GSUxUUkFSIiwiUk9MRV9ERVNQRVNBX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9DUklBUiIsIlJPTEVfUkVDRUlUQV9ERUxFVEFSIiwiUk9MRV9DT05UQVJFQ0VCRVJfREVMRVRBUiIsIlJPTEVfSE9NRV9SRVNVTFRBRE8iLCJST0xFX0RFU1BFU0FfREVMRVRBUiIsIlJPTEVfVVNVQVJJT19BVFVBTElaQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0FUVUFMSVpBUiIsIlJPTEVfQ09OVEFSRUNFQkVSX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0NSSUFSIiwiUk9MRV9DT05UQVBBR0FSX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQVRVQUxJWkFSIl0sImp0aSI6Ijg5NmI3NjVmLWE4YzAtNDJmMS1hZmY3LTU3MjEyZTIyMDIwNSIsImNsaWVudF9pZCI6ImFuZ3VsYXIifQ.9HKEDf0n2Y37uYP0jO482CHN1F1RrU_nd5i0lpgAFMQ";
	
	private Despesa despesa01;
	private Despesa despesa02;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private DespesaRepository despesaRepository;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/despesas";
		
		jsonDespesas_Despesa03_Create = ResourceUtils.getContentFromResource("/json/despesas/despesa-03-create.json");
		jsonDespesas_Despesa01_Update = ResourceUtils.getContentFromResource("/json/despesas/despesa-01-update.json");
		
		databaseCleaner.clearTables();
		prepararDados();
	}
	
	// Create
	@Test
	public void deveRetornarStatus201_QuandoCadastrarDespesa() {
		given()
			.auth().oauth2(token)
			.body(jsonDespesas_Despesa03_Create)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.CREATED.value());		
	}

	// FindAll
	@Test
	public void deveRetornarStatus200_QuandoConsultarDespesas() {		
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// FindAll - Quantidades de Despesas Existente
	@Test
	public void deveConterQuantidadeCorretaDeDespesas_QuandoConsultarDespesas() {		
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.body("", hasSize(quantidadeDespesas));
	}
	
	// FindById - Existente
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarDespesasExistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", despesa01.getId())
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("descricao", equalTo(despesa01.getDescricao()));
	}
	
	// FindById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoConsultarDespesasInexistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", DESPESA_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	//FindByStatusTrue
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarDespesasAtivas() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get("/ativo")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// DeleteById - Existente
	@Test
	public void deveRetornarStatus204_QuandoDeletarDespesaExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", despesa02.getId())
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NO_CONTENT.value());
	}
	
	// DeleteById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoDeletarDespesaInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", DESPESA_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// UpdateById - Existente
	@Test
	public void deveRetornarStatus200_QuandoAtualizarDespesaExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", despesa01.getId())
			.body(jsonDespesas_Despesa01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// UpdateById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoAtualizarDespesaInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", DESPESA_ID_INEXISTENTE)
			.body(jsonDespesas_Despesa01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	private void prepararDados() {
		despesa01 = new Despesa();
		despesa01.setDescricao("Teste Despesa 01");
		despesa01.setStatus(true);
		
		despesaRepository.save(despesa01);
		
		despesa02 = new Despesa();
		despesa02.setDescricao("Teste Despesa 02");
		despesa02.setStatus(true);
		
		despesaRepository.save(despesa02);
		
		quantidadeDespesas = (int) despesaRepository.count();
	}
	
}
