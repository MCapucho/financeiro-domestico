package br.com.systemmcr.sysfinan.testapi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.systemmcr.sysfinan.model.Receita;
import br.com.systemmcr.sysfinan.repository.ReceitaRepository;
import br.com.systemmcr.sysfinan.util.DatabaseCleaner;
import br.com.systemmcr.sysfinan.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class ApiReceitaIT {
	
	private static final int RECEITA_ID_INEXISTENTE = 100;
	
	private int quantidadeReceitas;
	
	private String jsonReceitas_Receita03_Create;
	private String jsonReceitas_Receita01_Update;
	
	private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzeXNmaW5hbi5zeXN0ZW1tY3JAZ21haWwuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5vbWUiOiJBZG1pbmlzdHJhZG9yIiwiZXhwIjoxNTc0OTY1NTA5LCJhdXRob3JpdGllcyI6WyJST0xFX0NPTlRBUEFHQVJfRklMVFJBUiIsIlJPTEVfVVNVQVJJT19GSUxUUkFSIiwiUk9MRV9VU1VBUklPX0NSSUFSIiwiUk9MRV9VU1VBUklPX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQ1JJQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9DUklBUiIsIlJPTEVfUkVDRUlUQV9GSUxUUkFSIiwiUk9MRV9ERVNQRVNBX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9DUklBUiIsIlJPTEVfUkVDRUlUQV9ERUxFVEFSIiwiUk9MRV9DT05UQVJFQ0VCRVJfREVMRVRBUiIsIlJPTEVfSE9NRV9SRVNVTFRBRE8iLCJST0xFX0RFU1BFU0FfREVMRVRBUiIsIlJPTEVfVVNVQVJJT19BVFVBTElaQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0FUVUFMSVpBUiIsIlJPTEVfQ09OVEFSRUNFQkVSX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0NSSUFSIiwiUk9MRV9DT05UQVBBR0FSX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQVRVQUxJWkFSIl0sImp0aSI6Ijg5NmI3NjVmLWE4YzAtNDJmMS1hZmY3LTU3MjEyZTIyMDIwNSIsImNsaWVudF9pZCI6ImFuZ3VsYXIifQ.9HKEDf0n2Y37uYP0jO482CHN1F1RrU_nd5i0lpgAFMQ";
	
	private Receita receita01;
	private Receita receita02;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private ReceitaRepository receitaRepository;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/receitas";
		
		jsonReceitas_Receita03_Create = ResourceUtils.getContentFromResource("/json/receitas/receita-03-create.json");
		jsonReceitas_Receita01_Update = ResourceUtils.getContentFromResource("/json/receitas/receita-01-update.json");
		
		databaseCleaner.clearTables();
		prepararDados();
	}
	
	// Create
	@Test
	public void deveRetornarStatus201_QuandoCadastrarReceita() {
		given()
			.auth().oauth2(token)
			.body(jsonReceitas_Receita03_Create)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.CREATED.value());		
	}

	// FindAll
	@Test
	public void deveRetornarStatus200_QuandoConsultarReceitas() {		
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// FindAll - Quantidades de Receitas Existente
	@Test
	public void deveConterQuantidadeCorretaDeReceitas_QuandoConsultarReceitas() {		
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.body("", hasSize(quantidadeReceitas));
	}
	
	// FindById - Existente
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarReceitasExistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", receita01.getId())
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("descricao", equalTo(receita01.getDescricao()));
	}
	
	// FindById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoConsultarReceitasInexistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", RECEITA_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	//FindByStatusTrue
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarReceitasAtivas() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get("/ativo")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// DeleteById - Existente
	@Test
	public void deveRetornarStatus204_QuandoDeletarReceitaExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", receita02.getId())
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NO_CONTENT.value());
	}
	
	// DeleteById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoDeletarReceitaInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", RECEITA_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// UpdateById - Existente
	@Test
	public void deveRetornarStatus200_QuandoAtualizarReceitaExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", receita01.getId())
			.body(jsonReceitas_Receita01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// UpdateById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoAtualizarReceitaInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", RECEITA_ID_INEXISTENTE)
			.body(jsonReceitas_Receita01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	private void prepararDados() {
		receita01 = new Receita();
		receita01.setDescricao("Teste Receita 01");
		receita01.setStatus(true);
		
		receitaRepository.save(receita01);
		
		receita02 = new Receita();
		receita02.setDescricao("Teste Receita 02");
		receita02.setStatus(true);
		
		receitaRepository.save(receita02);
		
		quantidadeReceitas = (int) receitaRepository.count();
	}
		
}
