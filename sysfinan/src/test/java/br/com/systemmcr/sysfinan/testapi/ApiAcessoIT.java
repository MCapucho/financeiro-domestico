package br.com.systemmcr.sysfinan.testapi;

import static io.restassured.RestAssured.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class ApiAcessoIT {

	private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzeXNmaW5hbi5zeXN0ZW1tY3JAZ21haWwuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5vbWUiOiJBZG1pbmlzdHJhZG9yIiwiZXhwIjoxNTc1Mzc3OTk2LCJhdXRob3JpdGllcyI6WyJST0xFX0NPTlRBUEFHQVJfRklMVFJBUiIsIlJPTEVfVVNVQVJJT19GSUxUUkFSIiwiUk9MRV9VU1VBUklPX0NSSUFSIiwiUk9MRV9VU1VBUklPX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQ1JJQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9DUklBUiIsIlJPTEVfUkVDRUlUQV9GSUxUUkFSIiwiUk9MRV9ERVNQRVNBX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9DUklBUiIsIlJPTEVfUkVDRUlUQV9ERUxFVEFSIiwiUk9MRV9DT05UQVJFQ0VCRVJfREVMRVRBUiIsIlJPTEVfSE9NRV9SRVNVTFRBRE8iLCJST0xFX0RFU1BFU0FfREVMRVRBUiIsIlJPTEVfVVNVQVJJT19BVFVBTElaQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0FUVUFMSVpBUiIsIlJPTEVfQ09OVEFSRUNFQkVSX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0NSSUFSIiwiUk9MRV9DT05UQVBBR0FSX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQVRVQUxJWkFSIl0sImp0aSI6IjE2OWM4YzRmLWQxMzYtNGFkMS04YTE2LTU5M2VmYzQ2ZDhhYSIsImNsaWVudF9pZCI6ImFuZ3VsYXIifQ.SQuHqZF0fbCnSCsC1izhYBwz34CxE2iE5h26MK9m-QY";
	
	@LocalServerPort
	private int port;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/acessos";
	}
	
	// FindAll
	@Test
	public void deveRetornarStatus200_QuandoConsultarAcessoUsuarioPermissao() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.OK.value());
	}
}
