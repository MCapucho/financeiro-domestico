package br.com.systemmcr.sysfinan.testapi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.Despesa;
import br.com.systemmcr.sysfinan.model.StatusTitulo;
import br.com.systemmcr.sysfinan.repository.ContaPagarRepository;
import br.com.systemmcr.sysfinan.repository.DespesaRepository;
import br.com.systemmcr.sysfinan.util.DatabaseCleaner;
import br.com.systemmcr.sysfinan.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class ApiContaPagarIT {

	private static final int CONTA_PAGAR_ID_INEXISTENTE = 100;
	
	private int quantidadeContasPagar;
	
	private String jsonContasPagar_ContaPagar03_Create;
	private String jsonContasPagar_ContaPagar01_Update;
	
	private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzeXNmaW5hbi5zeXN0ZW1tY3JAZ21haWwuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5vbWUiOiJBZG1pbmlzdHJhZG9yIiwiZXhwIjoxNTc0OTY1NTA5LCJhdXRob3JpdGllcyI6WyJST0xFX0NPTlRBUEFHQVJfRklMVFJBUiIsIlJPTEVfVVNVQVJJT19GSUxUUkFSIiwiUk9MRV9VU1VBUklPX0NSSUFSIiwiUk9MRV9VU1VBUklPX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQ1JJQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9DUklBUiIsIlJPTEVfUkVDRUlUQV9GSUxUUkFSIiwiUk9MRV9ERVNQRVNBX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9DUklBUiIsIlJPTEVfUkVDRUlUQV9ERUxFVEFSIiwiUk9MRV9DT05UQVJFQ0VCRVJfREVMRVRBUiIsIlJPTEVfSE9NRV9SRVNVTFRBRE8iLCJST0xFX0RFU1BFU0FfREVMRVRBUiIsIlJPTEVfVVNVQVJJT19BVFVBTElaQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0FUVUFMSVpBUiIsIlJPTEVfQ09OVEFSRUNFQkVSX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0NSSUFSIiwiUk9MRV9DT05UQVBBR0FSX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQVRVQUxJWkFSIl0sImp0aSI6Ijg5NmI3NjVmLWE4YzAtNDJmMS1hZmY3LTU3MjEyZTIyMDIwNSIsImNsaWVudF9pZCI6ImFuZ3VsYXIifQ.9HKEDf0n2Y37uYP0jO482CHN1F1RrU_nd5i0lpgAFMQ";
	
	private ContaPagar contaPagar01;
	private ContaPagar contaPagar02;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private ContaPagarRepository contaPagarRepository;
	
	@Autowired
	private DespesaRepository despesaRepository;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/contasPagar";
		
		jsonContasPagar_ContaPagar03_Create = ResourceUtils.getContentFromResource("/json/conta-pagar/conta-pagar-03-create.json");
		jsonContasPagar_ContaPagar01_Update = ResourceUtils.getContentFromResource("/json/conta-pagar/conta-pagar-01-update.json");
	
		databaseCleaner.clearTables();
		prepararDados();
	}
	
	// Create
	@Test
	public void deveRetornarStatus201_QuandoCadastrarContaPagar() {
		given()
			.auth().oauth2(token)
			.body(jsonContasPagar_ContaPagar03_Create)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	// FindAll
	@Test
	public void deveRetornarStatus200_QuandoConsultarContasPagar() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// FindAll - Quantidades de Receitas Existente
	@Test
	public void deveConterQuantidadeCorretaDeContasPagar_QuandoConsultarContasPagar() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.body("", hasSize(quantidadeContasPagar));
	}
	
	// FindById - Existente
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarContasPagarExistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", contaPagar01.getId())
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("numeroDocumento", equalTo(contaPagar01.getNumeroDocumento()));
	}
	
	// FindById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoConsultarContasPagarInexistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", CONTA_PAGAR_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// DeleteById - Existente
	@Test
	public void deveRetornarStatus204_QuandoDeletarContaPagarExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", contaPagar02.getId())
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NO_CONTENT.value());
	}
	
	// DeleteById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoDeletarContaPagarInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", CONTA_PAGAR_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// UpdateById - Existente
	@Test
	public void deveRetornarStatus200_QuandoAtualizarContaPagarExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", contaPagar01.getId())
			.body(jsonContasPagar_ContaPagar01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// UpdateById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoAtualizarContaPagarInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", CONTA_PAGAR_ID_INEXISTENTE)
			.body(jsonContasPagar_ContaPagar01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	private void prepararDados() {
		Despesa despesa = new Despesa();
		despesa.setDescricao("Teste Despesa");
		despesa.setStatus(true);
		
		despesaRepository.save(despesa);
				
		contaPagar01 = new ContaPagar();
		contaPagar01.setDataPagamento(LocalDate.of(2019, 11, 01));
		contaPagar01.setTipoDespesa(despesa);
		contaPagar01.setNumeroDocumento("123");
		contaPagar01.setValor(new BigDecimal("1230.00"));
		contaPagar01.setStatusTitulo(StatusTitulo.ABERTO);
		contaPagar01.setObservacao("Teste");
		
		contaPagarRepository.save(contaPagar01);
		
		contaPagar02 = new ContaPagar();
		contaPagar02.setDataPagamento(LocalDate.of(2019, 11, 01));
		contaPagar02.setTipoDespesa(despesa);
		contaPagar02.setNumeroDocumento("321");
		contaPagar02.setValor(new BigDecimal("3210.00"));
		contaPagar02.setStatusTitulo(StatusTitulo.ABERTO);
		contaPagar02.setObservacao("Teste");
		
		contaPagarRepository.save(contaPagar02);
		
		quantidadeContasPagar = (int) contaPagarRepository.count();
	}
}
