package br.com.systemmcr.sysfinan.testapi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.systemmcr.sysfinan.model.Usuario;
import br.com.systemmcr.sysfinan.repository.UsuarioRepository;
import br.com.systemmcr.sysfinan.util.DatabaseCleaner;
import br.com.systemmcr.sysfinan.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class ApiUsuarioIT {
	
	private static final int USUARIO_ID_INEXISTENTE = 100;
	
	private int quantidadeUsuarios;
	
	private String jsonUsuarios_Usuario03_Create;
	private String jsonUsuarios_Usuario02_Update;

	private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzeXNmaW5hbi5zeXN0ZW1tY3JAZ21haWwuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5vbWUiOiJBZG1pbmlzdHJhZG9yIiwiZXhwIjoxNTc0OTY1NTA5LCJhdXRob3JpdGllcyI6WyJST0xFX0NPTlRBUEFHQVJfRklMVFJBUiIsIlJPTEVfVVNVQVJJT19GSUxUUkFSIiwiUk9MRV9VU1VBUklPX0NSSUFSIiwiUk9MRV9VU1VBUklPX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQ1JJQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9DUklBUiIsIlJPTEVfUkVDRUlUQV9GSUxUUkFSIiwiUk9MRV9ERVNQRVNBX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9DUklBUiIsIlJPTEVfUkVDRUlUQV9ERUxFVEFSIiwiUk9MRV9DT05UQVJFQ0VCRVJfREVMRVRBUiIsIlJPTEVfSE9NRV9SRVNVTFRBRE8iLCJST0xFX0RFU1BFU0FfREVMRVRBUiIsIlJPTEVfVVNVQVJJT19BVFVBTElaQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0FUVUFMSVpBUiIsIlJPTEVfQ09OVEFSRUNFQkVSX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0NSSUFSIiwiUk9MRV9DT05UQVBBR0FSX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQVRVQUxJWkFSIl0sImp0aSI6Ijg5NmI3NjVmLWE4YzAtNDJmMS1hZmY3LTU3MjEyZTIyMDIwNSIsImNsaWVudF9pZCI6ImFuZ3VsYXIifQ.9HKEDf0n2Y37uYP0jO482CHN1F1RrU_nd5i0lpgAFMQ";
	
	private Usuario usuario01;
	private Usuario usuario02;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/usuarios";
		
		jsonUsuarios_Usuario03_Create = ResourceUtils.getContentFromResource("/json/usuarios/usuario-03-create.json");
		jsonUsuarios_Usuario02_Update = ResourceUtils.getContentFromResource("/json/usuarios/usuario-02-update.json");
		
		databaseCleaner.clearTables();
		prepararDados();
	}
	
	// Create
	@Test
	public void deveRetornarStatus201_QuandoCadastrarUsuario() {
		given()
			.auth().oauth2(token)
			.body(jsonUsuarios_Usuario03_Create)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	// FindAll
	@Test
	public void deveRetornarStatus200_QuandoConsultarUsuarios() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// FindAll - Quantidades de Receitas Existente
	@Test
	public void deveConterQuantidadeCorretaDeUsuarios_QuandoConsultarUsuarios() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.body("", hasSize(quantidadeUsuarios));
	}
	
	// FindById - Existente
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarUsuariosExistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", usuario01.getId())
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("nome", equalTo(usuario01.getNome()));
	}
	
	// FindById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoConsultarUsuariosInexistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", USUARIO_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// DeleteById - Existente
	@Test
	public void deveRetornarStatus204_QuandoDeletarUsuarioExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", usuario02.getId())
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NO_CONTENT.value());
	}
	
	// DeleteById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoDeletarReceitaInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", USUARIO_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// DeleteById - Administrador
	@Test
	public void deveRetornarStatus400_QuandoDeletarUsuarioAdministrador() {
		given()
			.auth().oauth2(token)
			.pathParam("id", usuario01.getId())
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.BAD_REQUEST.value());
	}
	
	// UpdateById - Existente
	@Test
	public void deveRetornarStatus200_QuandoAtualizarUsuarioExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", usuario02.getId())
			.body(jsonUsuarios_Usuario02_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// UpdateById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoAtualizarUsuarioInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", USUARIO_ID_INEXISTENTE)
			.body(jsonUsuarios_Usuario02_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	private void prepararDados() {
		usuario01 = new Usuario();
		usuario01.setNome("Teste Usuario 01");
		usuario01.setEmail("usuario01@sysfinan.com");
		usuario01.setSenha(encoder.encode("usuario01"));
		
		usuarioRepository.save(usuario01);
		
		usuario02 = new Usuario();
		usuario02.setNome("Teste Usuario 02");
		usuario02.setEmail("usuario02@sysfinan.com");
		usuario02.setSenha(encoder.encode("usuario02"));
		
		usuarioRepository.save(usuario02);
		
		quantidadeUsuarios = (int) usuarioRepository.count();
	}
	
}
