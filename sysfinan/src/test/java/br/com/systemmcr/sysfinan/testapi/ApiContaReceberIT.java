package br.com.systemmcr.sysfinan.testapi;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.model.Receita;
import br.com.systemmcr.sysfinan.model.StatusTitulo;
import br.com.systemmcr.sysfinan.repository.ContaReceberRepository;
import br.com.systemmcr.sysfinan.repository.ReceitaRepository;
import br.com.systemmcr.sysfinan.util.DatabaseCleaner;
import br.com.systemmcr.sysfinan.util.ResourceUtils;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/application-test.properties")
public class ApiContaReceberIT {

	private static final int CONTA_RECEBER_ID_INEXISTENTE = 100;
	
	private int quantidadeContasReceber;
	
	private String jsonContasReceber_ContaReceber03_Create;
	private String jsonContasReceber_ContaReceber01_Update;
	
	private String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJzeXNmaW5hbi5zeXN0ZW1tY3JAZ21haWwuY29tIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5vbWUiOiJBZG1pbmlzdHJhZG9yIiwiZXhwIjoxNTc0OTY1NTA5LCJhdXRob3JpdGllcyI6WyJST0xFX0NPTlRBUEFHQVJfRklMVFJBUiIsIlJPTEVfVVNVQVJJT19GSUxUUkFSIiwiUk9MRV9VU1VBUklPX0NSSUFSIiwiUk9MRV9VU1VBUklPX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQ1JJQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9DUklBUiIsIlJPTEVfUkVDRUlUQV9GSUxUUkFSIiwiUk9MRV9ERVNQRVNBX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9DUklBUiIsIlJPTEVfUkVDRUlUQV9ERUxFVEFSIiwiUk9MRV9DT05UQVJFQ0VCRVJfREVMRVRBUiIsIlJPTEVfSE9NRV9SRVNVTFRBRE8iLCJST0xFX0RFU1BFU0FfREVMRVRBUiIsIlJPTEVfVVNVQVJJT19BVFVBTElaQVIiLCJST0xFX0NPTlRBUkVDRUJFUl9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0FUVUFMSVpBUiIsIlJPTEVfQ09OVEFSRUNFQkVSX0FUVUFMSVpBUiIsIlJPTEVfREVTUEVTQV9GSUxUUkFSIiwiUk9MRV9SRUNFSVRBX0NSSUFSIiwiUk9MRV9DT05UQVBBR0FSX0RFTEVUQVIiLCJST0xFX0NPTlRBUEFHQVJfQVRVQUxJWkFSIl0sImp0aSI6Ijg5NmI3NjVmLWE4YzAtNDJmMS1hZmY3LTU3MjEyZTIyMDIwNSIsImNsaWVudF9pZCI6ImFuZ3VsYXIifQ.9HKEDf0n2Y37uYP0jO482CHN1F1RrU_nd5i0lpgAFMQ";
	
	private ContaReceber contaReceber01;
	private ContaReceber contaReceber02;
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private DatabaseCleaner databaseCleaner;
	
	@Autowired
	private ContaReceberRepository contaReceberRepository;
	
	@Autowired
	private ReceitaRepository receitaRepository;
	
	@Before
	public void setUp() {
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
		RestAssured.port = port;
		RestAssured.basePath = "/contasReceber";
		
		jsonContasReceber_ContaReceber03_Create = ResourceUtils.getContentFromResource("/json/conta-receber/conta-receber-03-create.json");
		jsonContasReceber_ContaReceber01_Update = ResourceUtils.getContentFromResource("/json/conta-receber/conta-receber-01-update.json");
	
		databaseCleaner.clearTables();
		prepararDados();
	}
	
	// Create
	@Test
	public void deveRetornarStatus201_QuandoCadastrarContaReceber() {
		given()
			.auth().oauth2(token)
			.body(jsonContasReceber_ContaReceber03_Create)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.post()
		.then()
			.statusCode(HttpStatus.CREATED.value());
	}
	
	// FindAll
	@Test
	public void deveRetornarStatus200_QuandoConsultarContasReceber() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// FindAll - Quantidades de Receitas Existente
	@Test
	public void deveConterQuantidadeCorretaDeContasReceber_QuandoConsultarContasReceber() {
		given()
			.auth().oauth2(token)
			.accept(ContentType.JSON)
		.when()
			.get()
		.then()
			.body("", hasSize(quantidadeContasReceber));
	}
	
	// FindById - Existente
	@Test
	public void deveRetornarRespostaEStatusCorretos_QuandoConsultarContasReceberExistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", contaReceber01.getId())
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value())
			.body("numeroDocumento", equalTo(contaReceber01.getNumeroDocumento()));
	}
	
	// FindById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoConsultarContasReceberInexistentes() {
		given()
			.auth().oauth2(token)
			.pathParam("id", CONTA_RECEBER_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.get("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// DeleteById - Existente
	@Test
	public void deveRetornarStatus204_QuandoDeletarContaReceberExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", contaReceber02.getId())
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NO_CONTENT.value());
	}
	
	// DeleteById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoDeletarContaReceberInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", CONTA_RECEBER_ID_INEXISTENTE)
			.accept(ContentType.JSON)
		.when()
			.delete("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	// UpdateById - Existente
	@Test
	public void deveRetornarStatus200_QuandoAtualizarContaReceberExistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", contaReceber01.getId())
			.body(jsonContasReceber_ContaReceber01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.OK.value());
	}
	
	// UpdateById - Inexistente
	@Test
	public void deveRetornarStatus404_QuandoAtualizarContaReceberInexistente() {
		given()
			.auth().oauth2(token)
			.pathParam("id", CONTA_RECEBER_ID_INEXISTENTE)
			.body(jsonContasReceber_ContaReceber01_Update)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.put("/{id}")
		.then()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}
	
	private void prepararDados() {		
		Receita receita = new Receita();
		receita.setDescricao("Teste");
		receita.setStatus(true);
		
		receitaRepository.save(receita);
				
		contaReceber01 = new ContaReceber();
		contaReceber01.setDataRecebimento(LocalDate.of(2019, 11, 01));
		contaReceber01.setTipoReceita(receita);
		contaReceber01.setNumeroDocumento("123");
		contaReceber01.setValor(new BigDecimal("1230.00"));
		contaReceber01.setStatusTitulo(StatusTitulo.ABERTO);
		contaReceber01.setObservacao("Teste");
		
		contaReceberRepository.save(contaReceber01);
		
		contaReceber02 = new ContaReceber();
		contaReceber02.setDataRecebimento(LocalDate.of(2019, 11, 01));
		contaReceber02.setTipoReceita(receita);
		contaReceber02.setNumeroDocumento("321");
		contaReceber02.setValor(new BigDecimal("3210.00"));
		contaReceber02.setStatusTitulo(StatusTitulo.ABERTO);
		contaReceber02.setObservacao("Teste");
		
		contaReceberRepository.save(contaReceber02);
		
		quantidadeContasReceber = (int) contaReceberRepository.count();
	}
}
