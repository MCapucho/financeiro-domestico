CREATE TABLE permissao (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	descricao VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO permissao (id, descricao) values (1, 'ROLE_RECEITA_CRIAR');
INSERT INTO permissao (id, descricao) values (2, 'ROLE_RECEITA_FILTRAR');
INSERT INTO permissao (id, descricao) values (3, 'ROLE_RECEITA_DELETAR');
INSERT INTO permissao (id, descricao) values (4, 'ROLE_RECEITA_ATUALIZAR');

INSERT INTO permissao (id, descricao) values (5, 'ROLE_DESPESA_CRIAR');
INSERT INTO permissao (id, descricao) values (6, 'ROLE_DESPESA_FILTRAR');
INSERT INTO permissao (id, descricao) values (7, 'ROLE_DESPESA_DELETAR');
INSERT INTO permissao (id, descricao) values (8, 'ROLE_DESPESA_ATUALIZAR');

INSERT INTO permissao (id, descricao) values (9, 'ROLE_CONTARECEBER_CRIAR');
INSERT INTO permissao (id, descricao) values (10, 'ROLE_CONTARECEBER_FILTRAR');
INSERT INTO permissao (id, descricao) values (11, 'ROLE_CONTARECEBER_DELETAR');
INSERT INTO permissao (id, descricao) values (12, 'ROLE_CONTARECEBER_ATUALIZAR');

INSERT INTO permissao (id, descricao) values (13, 'ROLE_CONTAPAGAR_CRIAR');
INSERT INTO permissao (id, descricao) values (14, 'ROLE_CONTAPAGAR_FILTRAR');
INSERT INTO permissao (id, descricao) values (15, 'ROLE_CONTAPAGAR_DELETAR');
INSERT INTO permissao (id, descricao) values (16, 'ROLE_CONTAPAGAR_ATUALIZAR');

INSERT INTO permissao (id, descricao) values (17, 'ROLE_USUARIO_CRIAR');
INSERT INTO permissao (id, descricao) values (18, 'ROLE_USUARIO_FILTRAR');
INSERT INTO permissao (id, descricao) values (19, 'ROLE_USUARIO_DELETAR');
INSERT INTO permissao (id, descricao) values (20, 'ROLE_USUARIO_ATUALIZAR');

INSERT INTO permissao (id, descricao) values (21, 'ROLE_HOME_RESULTADO');