CREATE TABLE usuario_permissao (
	usuario_id BIGINT(20) NOT NULL,
	permissao_id BIGINT(20) NOT NULL,
	FOREIGN KEY (usuario_id) REFERENCES usuario(id),
	FOREIGN KEY (permissao_id) REFERENCES permissao(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- admin
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 1);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 2);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 3);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 4);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 5);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 6);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 7);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 8);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 9);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 10);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 11);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 12);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 13);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 14);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 15);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 16);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 17);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 18);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 19);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 20);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (1, 21);

INSERT INTO usuario_permissao (usuario_id, permissao_id) values (2, 2);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (2, 6);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (2, 10);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (2, 14);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (2, 18);
INSERT INTO usuario_permissao (usuario_id, permissao_id) values (2, 21);