CREATE TABLE conta_receber (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	data_recebimento DATE,
	numero_documento VARCHAR(10),
	valor DECIMAL(10,2) NOT NULL,
	observacao VARCHAR(200),
	status_titulo VARCHAR(15),
	codigo_receita BIGINT(20) NOT NULL,
	FOREIGN KEY (codigo_receita) REFERENCES receita(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;