CREATE TABLE usuario (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(100) NOT NULL,
	senha VARCHAR(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO usuario (id, nome, email, senha) values (1, 'Administrador', 'sysfinan.systemmcr@gmail.com', '$2a$10$kN8ED50mp.GjaxWWVmVbbuVvd1N/KjXCjaFpy7OlW.Gk5a15B6avG');
INSERT INTO usuario (id, nome, email, senha) values (2, 'Usuario', 'usuario@sysfinan.com', '$2a$10$sF3BeeFkeWOQOEet2DJ.SOJ2XwMIo0EUAdxx3U0Mbk2X3B97vH.dy');