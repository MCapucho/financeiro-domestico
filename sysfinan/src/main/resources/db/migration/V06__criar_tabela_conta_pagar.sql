CREATE TABLE conta_pagar (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	data_pagamento DATE,
	numero_documento VARCHAR(10),
	valor DECIMAL(10,2) NOT NULL,
	observacao VARCHAR(200),
	status_titulo VARCHAR(15),
	codigo_despesa BIGINT(20) NOT NULL,
	FOREIGN KEY (codigo_despesa) REFERENCES despesa(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;