package br.com.systemmcr.sysfinan.dto;

import br.com.systemmcr.sysfinan.model.Receita;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceitaDTO {

	private Long id;
	private String descricao;
	
	// Construtor
	public ReceitaDTO() {
		
	}
	
	public ReceitaDTO(Receita receita) {
		this();
		this.id = receita.getId();
		this.descricao = receita.getDescricao();
	}
		
}
