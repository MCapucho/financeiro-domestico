package br.com.systemmcr.sysfinan.util;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.context.annotation.Configuration;

import br.com.systemmcr.sysfinan.exception.EmailInvalidoException;

@Configuration
public class ValidationEmail {

	public boolean isValidEmailAddress(String email) {
	    boolean result = true;
	    try {
	        InternetAddress emailAddr = new InternetAddress(email);
	        emailAddr.validate();
	    } catch (AddressException ex) {
	    	throw new EmailInvalidoException("E-mail Inválido!!");
	    }
	    return result;
	}
	
}
