package br.com.systemmcr.sysfinan.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.systemmcr.sysfinan.dto.ContasDTO;
import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.model.StatusTitulo;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@Service
public class ResultadoService {

	@Autowired
	private ContaPagarService contaPagarService;
	
	@Autowired
	private ContaReceberService contaReceberService;
	
	public ContasDTO contasFiltradas(LocalDate dataInicial, LocalDate dataFinal) {
		FilterPageable filterPageablePagar = new FilterPageable(0, 10, "dataPagamento", "ASC");
		FilterPageable filterPageableReceber = new FilterPageable(0, 10, "dataRecebimento", "ASC");
		
		Page<ContaPagar> contasPagar = contaPagarService.findByDataPagamentoBetweenCP(dataInicial,
																					  dataFinal, 
																					  filterPageablePagar);
		
		Page<ContaReceber> contasReceber = contaReceberService.findByDataPagamentoBetweenCR(dataInicial,
																						    dataFinal,
																						    filterPageableReceber);
		
		ContasDTO contasDTO = new ContasDTO();
		
		List<ContaPagar> contasPagarAberta = contasPagar.stream()
				                                        .filter(contaPagar -> contaPagar.getStatusTitulo().equals(StatusTitulo.ABERTO))
				                                        .collect(Collectors.toList());
				                      
		List<ContaPagar> contasPagarQuitada = contasPagar.stream()
				                                         .filter(contaPagar -> contaPagar.getStatusTitulo().equals(StatusTitulo.QUITADO))
				                                         .collect(Collectors.toList());
		
		List<ContaReceber> contasReceberAberta = contasReceber.stream()
				                                              .filter(contaReceber -> contaReceber.getStatusTitulo().equals(StatusTitulo.ABERTO))
				                                              .collect(Collectors.toList());
		
		List<ContaReceber> contasReceberQuitada = contasReceber.stream()
				                                               .filter(contaReceber -> contaReceber.getStatusTitulo().equals(StatusTitulo.QUITADO))
				                                               .collect(Collectors.toList());
		
		contasDTO.setContasDespesasAbertas(contasPagarAberta);
		contasDTO.setContasDespesasQuitadas(contasPagarQuitada);
		contasDTO.setContasReceitasAbertas(contasReceberAberta);
		contasDTO.setContasReceitasQuitadas(contasReceberQuitada);
		
		return contasDTO;
		
	}
	
}
