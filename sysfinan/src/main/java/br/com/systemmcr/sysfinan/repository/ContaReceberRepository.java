package br.com.systemmcr.sysfinan.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.model.StatusTitulo;

public interface ContaReceberRepository extends JpaRepository<ContaReceber, Long> {

	List<ContaReceber> monthlyTotalizerCR(@Param("month") int mes);
	
	Page<ContaReceber> findByDataRecebimentoBetween(@Param("dataInicial") LocalDate dataInicial, 
			                                        @Param("dataFinal") LocalDate dataFinal,
			                                        Pageable pageable);
	
	List<ContaReceber> findByDataRecebimentoLessThanEqualAndStatusTitulo(LocalDate dataRecebimento, 
            														   StatusTitulo statusTitulo);
	
}
