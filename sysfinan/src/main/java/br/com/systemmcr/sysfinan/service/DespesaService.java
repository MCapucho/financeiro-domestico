package br.com.systemmcr.sysfinan.service;

import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_DESPESA_EM_USO;
import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_DESPESA_EXISTENTE;
import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_DESPESA_PROIBIDO_ALTERACAO;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.systemmcr.sysfinan.exception.DadosDuplicadosException;
import br.com.systemmcr.sysfinan.exception.DespesaNaoEncontradaException;
import br.com.systemmcr.sysfinan.exception.EntidadeEmUsoException;
import br.com.systemmcr.sysfinan.exception.ReceitaNaoEncontradaException;
import br.com.systemmcr.sysfinan.model.Despesa;
import br.com.systemmcr.sysfinan.repository.DespesaRepository;
import br.com.systemmcr.sysfinan.util.FilterPageable;;

@Service
public class DespesaService {
	
	@Autowired
	private DespesaRepository despesaRepository;
	
	public Despesa create(Despesa despesa) {
		Despesa despesaNova = despesaRepository.findByDescricao(despesa.getDescricao());
		
		if (despesaNova == null) {
			return despesaRepository.save(despesa);
		} else {
			throw new DadosDuplicadosException(MSG_DESPESA_EXISTENTE);
		}
	}
	
	public Page<Despesa> findByPage(FilterPageable filterPageable) {
        return despesaRepository.findAll(filterPageable.listByPage());
    }
	
	public Despesa findById(Long id) {
		return despesaRepository.findById(id).orElseThrow(() -> new DespesaNaoEncontradaException(id));
	}

	public Despesa update(Long id, Despesa despesa) {
		Despesa despesaSalva = findById(id);
		
		BeanUtils.copyProperties(despesa, despesaSalva, "id", "contasPagar");
		
		if(despesaSalva.getContasPagar().isEmpty()) {
			despesaSalva.setStatus(despesaSalva.getStatus());
			
			return despesaRepository.save(despesaSalva);
		} else {
			throw new EntidadeEmUsoException(MSG_DESPESA_PROIBIDO_ALTERACAO);
		}				
	}
	
	public void deleteById(Long id) {
		try {
			despesaRepository.deleteById(id);
			
		} catch (EmptyResultDataAccessException e) {
			throw new ReceitaNaoEncontradaException(id);
		
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
				String.format(MSG_DESPESA_EM_USO, id));
		}
	}
		
}