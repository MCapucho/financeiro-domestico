package br.com.systemmcr.sysfinan.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.systemmcr.sysfinan.exception.ContaPagarNaoEncontradaException;
import br.com.systemmcr.sysfinan.exception.ContaReceberNaoEncontradaException;
import br.com.systemmcr.sysfinan.mail.Mailer;
import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.StatusTitulo;
import br.com.systemmcr.sysfinan.model.Usuario;
import br.com.systemmcr.sysfinan.repository.ContaPagarRepository;
import br.com.systemmcr.sysfinan.repository.UsuarioRepository;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@Service
public class ContaPagarService {
	
	private static final Logger logger = LoggerFactory.getLogger(ContaPagarService.class);

	@Autowired
	private ContaPagarRepository contaPagarRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private Mailer mailer;
	
	public ContaPagar create(ContaPagar contaPagar) {
		contaPagar.setStatusTitulo(StatusTitulo.ABERTO);
		
		return contaPagarRepository.save(contaPagar);
	}
	
	public List<ContaPagar> findAll() {
		return contaPagarRepository.findAll();
	}
	
	public Page<ContaPagar> findByPage(FilterPageable filterPageable) {
		return contaPagarRepository.findAll(filterPageable.listByPage());
	}
	
	public ContaPagar findById(Long id) {
		return contaPagarRepository.findById(id).orElseThrow(() -> new ContaReceberNaoEncontradaException(id));
	}
	
	public void deleteById(Long id) {
		try {
			contaPagarRepository.deleteById(id);
			
		} catch (EmptyResultDataAccessException e) {
			throw new ContaPagarNaoEncontradaException(id);
		}
	}
			
	public ContaPagar update(Long id, ContaPagar contaPagar) {
		ContaPagar contaPagarSalva = findById(id);
		
		BeanUtils.copyProperties(contaPagar, contaPagarSalva, "id");
		
		return contaPagarRepository.save(contaPagarSalva);
	}
	
	public Double monthlyTotalizerCP() {
		List<ContaPagar> conta = contaPagarRepository.monthlyTotalizerCP(LocalDate.now().getMonth().getValue());
		
		Double total = conta.stream()
							.mapToDouble(obj -> obj.getValor().doubleValue())
							.sum();
		return total;
	}

	public Double missingToPay() {
		List<ContaPagar> conta = contaPagarRepository.missingToPayCP(LocalDate.now());
		
		Double total = conta.stream()
				            .mapToDouble(obj -> obj.getValor().doubleValue())
				            .sum();
		return total;
	}
	
	public Page<ContaPagar> findByDataPagamentoBetweenCP(LocalDate dataInicial, LocalDate dataFinal, FilterPageable filterPageable) {
		Page<ContaPagar> resultado = contaPagarRepository.findByDataPagamentoBetween(dataInicial, 
				                                                                     dataFinal, 
				                                                                     filterPageable.listByPage());
		
		return resultado;
	}
		
	@Scheduled(cron = "0 0 9 * * *")
	public void avisarSobreContaPagarVencidos() {
		if ( logger.isDebugEnabled()) {
			logger.debug("Preparando envio de e-mails de aviso de contas à pagar");
		}
		
		List<ContaPagar> vencidos = contaPagarRepository.findByDataPagamentoLessThanEqualAndStatusTitulo(LocalDate.now(), StatusTitulo.ABERTO);
		
		if (vencidos.isEmpty()) {
			logger.info("Contas à pagar em dia!");
			
			return;
		}
		
		logger.info("Existem {} contas à pagar vencidos", vencidos.size());
		
		List<Usuario> destinatarios = usuarioRepository.findAll();
		
		if (destinatarios.isEmpty()) {
			logger.warn("Existem contas à pagar, porém o sistema não encontrou destinatários");
			
			return;
		}
		
		mailer.avisarSobreContaPagarVencidos(vencidos, destinatarios);
		
		logger.info("Envio de e-mail de aviso concluído!");
	}
	
}
