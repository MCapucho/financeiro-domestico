package br.com.systemmcr.sysfinan.model;

public enum StatusTitulo {

	ABERTO("Aberto"),
	QUITADO("Quitado");
	
	private String descricao;

	// Construtor
	private StatusTitulo(String descricao) {
		this.descricao = descricao;
	}

	// Getters e Setters
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
		
}
