package br.com.systemmcr.sysfinan.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.service.ContaPagarService;
import br.com.systemmcr.sysfinan.service.ContaReceberService;

@RestController
@RequestMapping("/home")
public class HomeResource {
		
	@Autowired
	private ContaPagarService contaPagarService;
	
	@Autowired
	private ContaReceberService contaReceberService;
					
	// Somatória de conta à receber
	@GetMapping("/totalContaReceber")
	@PreAuthorize("hasAuthority('ROLE_HOME_RESULTADO') and #oauth2.hasScope('read')")
	public Double totalizadorContaReceber() {
		return contaReceberService.monthlyTotalizerCR();
	}
	
	// Somatória de conta à pagar
	@GetMapping("/totalContaPagar")
	@PreAuthorize("hasAuthority('ROLE_HOME_RESULTADO') and #oauth2.hasScope('read')")
	public Double totalizadorContaPagar( ) {
		return contaPagarService.monthlyTotalizerCP();
	}
	
	@GetMapping("/totalFinal")
	@PreAuthorize("hasAuthority('ROLE_HOME_RESULTADO') and #oauth2.hasScope('read')")
	public Double totalizadorFinal() {
		Double total = contaReceberService.monthlyTotalizerCR() - contaPagarService.monthlyTotalizerCP();
		
		return total;
	}
	
	@GetMapping("/todoPeriodoContaPagar")
	@PreAuthorize("hasAuthority('ROLE_HOME_RESULTADO') and #oauth2.hasScope('read')")
	public Double totalizadorTodoPeriodoContaPagar( ) {
		return contaPagarService.missingToPay();
	}
	
}