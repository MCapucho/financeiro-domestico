package br.com.systemmcr.sysfinan.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.StatusTitulo;

public interface ContaPagarRepository extends JpaRepository<ContaPagar, Long> {
	
	List<ContaPagar> monthlyTotalizerCP(@Param("month") int mes);

	List<ContaPagar> missingToPayCP(@Param ("date") LocalDate data);
		
	Page<ContaPagar> findByDataPagamentoBetween(@Param("dataInicial") LocalDate dataInicial, 
			                                    @Param("dataFinal") LocalDate dataFinal,
			                                    Pageable pageable);
	
	List<ContaPagar> findByDataPagamentoLessThanEqualAndStatusTitulo(LocalDate dataPagamento, 
			                                                         StatusTitulo statusTitulo);
	
}
