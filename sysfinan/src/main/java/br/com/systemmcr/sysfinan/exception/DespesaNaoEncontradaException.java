package br.com.systemmcr.sysfinan.exception;

public class DespesaNaoEncontradaException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public DespesaNaoEncontradaException(String mensagem) {
		super(mensagem);
	}
	
	public DespesaNaoEncontradaException(Long id) {
		this(String.format("Não existe uma despesa com código %d", id));
	}
	
}
