package br.com.systemmcr.sysfinan.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import br.com.systemmcr.sysfinan.config.property.SysfinanApiProperty;

@Configuration
public class MailConfig {
	
	@Autowired
	private SysfinanApiProperty sysfinanApiProperty;

	@Bean
	public JavaMailSender javaMailSender() {
		Properties properties = new Properties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.oauth", true);
		properties.put("mail.smtp.starttls.enable", true);
		properties.put("mail.smtp.connectiontimeout", 10000);
		
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setJavaMailProperties(properties);
		mailSender.setHost(sysfinanApiProperty.getMail().getHost());
		mailSender.setPort(sysfinanApiProperty.getMail().getPort());
		mailSender.setUsername(sysfinanApiProperty.getMail().getUsername());
		mailSender.setPassword(sysfinanApiProperty.getMail().getPassword());
				
		return mailSender;
	}
	
}
