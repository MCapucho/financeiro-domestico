package br.com.systemmcr.sysfinan.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import br.com.systemmcr.sysfinan.exception.DadosDuplicadosException;
import br.com.systemmcr.sysfinan.exception.EntidadeEmUsoException;
import br.com.systemmcr.sysfinan.exception.ReceitaNaoEncontradaException;
import br.com.systemmcr.sysfinan.model.Receita;
import br.com.systemmcr.sysfinan.repository.ReceitaRepository;
import br.com.systemmcr.sysfinan.util.FilterPageable;

import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_RECEITA_EM_USO;
import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_RECEITA_EXISTENTE;
import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_RECEITA_PROIBIDO_ALTERACAO;

@Service
public class ReceitaService {
		
	@Autowired
	private ReceitaRepository receitaRepository;
	
	public Receita create(Receita receita) {		
		Receita receitaNova = receitaRepository.findByDescricao(receita.getDescricao());
					
		if (receitaNova == null) {
			return receitaRepository.save(receita);
		} else {
			throw new DadosDuplicadosException(MSG_RECEITA_EXISTENTE);
		}
	}
	
	public Page<Receita> findByPage(FilterPageable filterPageable) {
        return receitaRepository.findAll(filterPageable.listByPage());
    } 
	
	public Receita findById(Long id) {
		return receitaRepository.findById(id).orElseThrow(() -> new ReceitaNaoEncontradaException(id));		
	}
	
	public Receita update(Long id, Receita receita) {
		Receita receitaSalva = findById(id);
		
		BeanUtils.copyProperties(receita, receitaSalva, "id", "contasReceber");

		if(receitaSalva.getContasReceber().isEmpty()) {
			receitaSalva.setStatus(receitaSalva.getStatus());
			
			return receitaRepository.save(receitaSalva);
		} else {
			throw new EntidadeEmUsoException(MSG_RECEITA_PROIBIDO_ALTERACAO);
		}					
	}
	
	public void deleteById(Long id) {
		try {
			receitaRepository.deleteById(id);
			
		} catch (EmptyResultDataAccessException e) {
			throw new ReceitaNaoEncontradaException(id);
		
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException(
				String.format(MSG_RECEITA_EM_USO, id));
		}
	}
	
}
