package br.com.systemmcr.sysfinan.service;

import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_USUARIO_ADM_PROIBIDO_EXCLUIR;
import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_USUARIO_EMAIL_EXISTENTE;
import static br.com.systemmcr.sysfinan.util.MessageValidation.MSG_USUARIO_PERMISSAO_EXISTENTE;

import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.systemmcr.sysfinan.dto.UsuarioPermissaoDTO;
import br.com.systemmcr.sysfinan.exception.DadosDuplicadosException;
import br.com.systemmcr.sysfinan.exception.UsuarioNaoEncontradoException;
import br.com.systemmcr.sysfinan.exception.UsuarioPadraoException;
import br.com.systemmcr.sysfinan.model.Permissao;
import br.com.systemmcr.sysfinan.model.Usuario;
import br.com.systemmcr.sysfinan.repository.UsuarioRepository;
import br.com.systemmcr.sysfinan.util.FilterPageable;
import br.com.systemmcr.sysfinan.util.ValidationEmail;

@Service
public class UsuarioService {
		
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private ValidationEmail validationEmail;

	@Autowired
	private UsuarioRepository usuarioRepository;
		
	public Usuario create(Usuario usuario) {
		Usuario usuarioNovo = usuarioRepository.findByUsuarioExistente(usuario.getEmail());
				
		if (usuarioNovo == null) {
			validationEmail.isValidEmailAddress(usuario.getEmail());
			usuario.setSenha(encoder.encode(usuario.getSenha()));
			return usuarioRepository.save(usuario);
		} else {
			throw new DadosDuplicadosException(MSG_USUARIO_EMAIL_EXISTENTE);
		}		
	}
	
	public Page<Usuario> findByPage(FilterPageable filterPageable) {
		return usuarioRepository.findAll(filterPageable.listByPage());
	}
	
	public Page<UsuarioPermissaoDTO> findByPagePermissao(FilterPageable filterPageable) {
		return usuarioRepository.findByUsuarioPermissao(filterPageable.listByPage());
	}
	
	public Usuario findById(Long id) {
		return usuarioRepository.findById(id).orElseThrow(() -> new UsuarioNaoEncontradoException(id));		
	}
	
	public Usuario update(Long id, Usuario usuario) {		
		Usuario usuarioSalvo = findById(id);
		
		BeanUtils.copyProperties(usuario, usuarioSalvo, "id", "senha");
		
		validationEmail.isValidEmailAddress(usuarioSalvo.getEmail());
						
		return usuarioRepository.save(usuarioSalvo);
	}
	
	public Usuario updatePassword(Long id, Usuario usuario) {
		Usuario usuarioSalvo = findById(id);
	
		usuario.setSenha(encoder.encode(usuario.getSenha()));
		
		BeanUtils.copyProperties(usuario, usuarioSalvo, "id", "nome", "email");
		
		return usuarioRepository.save(usuarioSalvo);
	}
	
	public Usuario updateAcess(Long id, Usuario usuario) {
		Usuario usuarioSalvo = findById(id);
		
		Set<Permissao> permissoesSalva = usuarioSalvo.getPermissoes();
		Set<Permissao> permissoes = usuario.getPermissoes();
		
		permissoes.forEach(permissao -> {
			if (permissoesSalva.contains(permissao)) {
				throw new DadosDuplicadosException(MSG_USUARIO_PERMISSAO_EXISTENTE);
			}
		});
		
		permissoes.forEach(permissao -> usuarioSalvo.getPermissoes().add(permissao));
		
		return usuarioRepository.save(usuarioSalvo);
	}
		
	public void deleteById(Long id) {
		Usuario usuario = findById(id);
		
		if (usuario != null) {			
			if (usuario.getId() != 1) {
				usuarioRepository.deleteById(id);
			} else {			
				throw new UsuarioPadraoException(MSG_USUARIO_ADM_PROIBIDO_EXCLUIR);
			}
		} else {
			throw new UsuarioNaoEncontradoException(id);
		}
	}
		
}
