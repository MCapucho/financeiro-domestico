package br.com.systemmcr.sysfinan.mail;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.model.Usuario;

@Component
public class Mailer {

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine thymeleaf;
	
	public void avisarSobreContaPagarVencidos(List<ContaPagar> vencidos, List<Usuario> destinatarios) {		
		Map<String, Object> variaveis = new HashMap<>();
		variaveis.put("contasPagar", vencidos);
		
		List<String> emails = destinatarios.stream().map(u -> u.getEmail()).collect(Collectors.toList());
		
		this.enviarEmail("sysfinan.systemmcr@gmail.com", 
				         emails, 
				         "Contas à Pagar - Vencidos", 
				         "mail/conta-pagar-vencidos", 
				         variaveis);
	}
	
	public void avisarSobreContaReceberEmAberto(List<ContaReceber> abertos, List<Usuario> destinatarios) {		
		Map<String, Object> variaveis = new HashMap<>();
		variaveis.put("contasReceber", abertos);
		
		List<String> emails = destinatarios.stream().map(u -> u.getEmail()).collect(Collectors.toList());
		
		this.enviarEmail("sysfinan.systemmcr@gmail.com", 
				         emails, 
				         "Contas à Receber - Em Aberto", 
				         "mail/conta-receber-em-aberto", 
				         variaveis);
	}

	public void enviarEmail(String remetente, List<String> destinatarios, String assunto, String template,
			Map<String, Object> variaveis) {
		Context context = new Context(new Locale("pt", "BR"));

		variaveis.entrySet().forEach(e -> context.setVariable(e.getKey(), e.getValue()));

		String mensagem = thymeleaf.process(template, context);

		this.enviarEmail(remetente, destinatarios, assunto, mensagem);
	}

	public void enviarEmail(String remetente, List<String> destinatarios, String assunto, String mensagem) {
		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();

			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
			mimeMessageHelper.setFrom(remetente);
			mimeMessageHelper.setTo(destinatarios.toArray(new String[destinatarios.size()]));
			mimeMessageHelper.setSubject(assunto);
			mimeMessageHelper.setText(mensagem, true);

			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			throw new RuntimeException("Problemas com o envio de e-mail", e);
		}
	}

}
