package br.com.systemmcr.sysfinan.exception;

public class EmailInvalidoException extends NegocioException {

	private static final long serialVersionUID = 1L;

	public EmailInvalidoException(String mensagem) {
		super(mensagem);
	}
	
}
