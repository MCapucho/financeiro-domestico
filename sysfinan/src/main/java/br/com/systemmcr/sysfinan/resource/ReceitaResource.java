package br.com.systemmcr.sysfinan.resource;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.dto.ReceitaDTO;
import br.com.systemmcr.sysfinan.model.Receita;
import br.com.systemmcr.sysfinan.repository.ReceitaRepository;
import br.com.systemmcr.sysfinan.service.ReceitaService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/receitas")
public class ReceitaResource {

	@Autowired
	private ReceitaRepository receitaRepository;
	
	@Autowired
	private ReceitaService receitaService;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_RECEITA_CRIAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Receita> create(@Valid @RequestBody Receita receita) {	
		Receita receitaNova = receitaService.create(receita);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(receitaNova);
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_RECEITA_FILTRAR') and #oauth2.hasScope('read')")
	public List<Receita> findAll() {
		return receitaRepository.findAll();
	}
	
	@GetMapping("/page")
	@PreAuthorize("hasAuthority('ROLE_RECEITA_FILTRAR') and #oauth2.hasScope('read')")
    public Page<Receita> findByPage(FilterPageable filterPageable) {
        return receitaService.findByPage(filterPageable);
    }
	
	@GetMapping("/ativo")
	@PreAuthorize("hasAuthority('ROLE_RECEITA_FILTRAR') and #oauth2.hasScope('read')")
	public List<ReceitaDTO> findByStatusAtivo() {
		return receitaRepository.findByStatusTrue().stream()
												   .map(receita -> new ReceitaDTO(receita))
												   .collect(Collectors.toList());
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_RECEITA_FILTRAR') and #oauth2.hasScope('read')")
	public ResponseEntity<Receita> findById(@PathVariable Long id) {
		Receita receita = receitaService.findById(id);
		
		return receita != null ? ResponseEntity.ok(receita) : ResponseEntity.notFound().build();
	}
		
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_RECEITA_DELETAR') and #oauth2.hasScope('write')")
	public void delete(@PathVariable Long id) {
		receitaService.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_RECEITA_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Receita> update(@PathVariable Long id, @Valid @RequestBody Receita receita) {
		Receita receitaSalva = receitaService.update(id, receita);
		
		return ResponseEntity.ok(receitaSalva);
	}
	
}
