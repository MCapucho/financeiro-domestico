package br.com.systemmcr.sysfinan.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "conta_pagar")
public class ContaPagar {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Column(name = "data_pagamento")
	private LocalDate dataPagamento;
	
	@ManyToOne
	@JoinColumn(name = "codigo_despesa")
	private Despesa tipoDespesa;
	
	@Column(name = "numero_documento")
	private String numeroDocumento;
	
	@NotNull
	private BigDecimal valor;
	
	@Size(max = 200)
	private String observacao;
	
	@Enumerated(EnumType.STRING)
	private StatusTitulo statusTitulo;
	
	@JsonIgnore
	public boolean isStatus() {
		return StatusTitulo.ABERTO.equals(this.statusTitulo);
	}
	
}
