package br.com.systemmcr.sysfinan.exception;

public class ContaPagarNaoEncontradaException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public ContaPagarNaoEncontradaException(String mensagem) {
		super(mensagem);
	}
	
	public ContaPagarNaoEncontradaException(Long id) {
		this(String.format("Não existe uma conta à pagar com código %d", id));
	}
	
}
