package br.com.systemmcr.sysfinan.exception;

public class ContaReceberNaoEncontradaException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public ContaReceberNaoEncontradaException(String mensagem) {
		super(mensagem);
	}
	
	public ContaReceberNaoEncontradaException(Long id) {
		this(String.format("Não existe uma conta à receber com código %d", id));
	}
	
}
