package br.com.systemmcr.sysfinan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.systemmcr.sysfinan.model.Receita;

public interface ReceitaRepository extends JpaRepository<Receita, Long> {
	
	Receita findByDescricao(String descricao);
	
	List<Receita> findByStatusTrue();
		
}
