package br.com.systemmcr.sysfinan.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "despesa")
public class Despesa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(max = 100)
	private String descricao;
	
	@NotNull
	private Boolean status;
	
	@OneToMany(mappedBy = "tipoDespesa", fetch = FetchType.LAZY)
	@JsonIgnoreProperties(value = {"tipoDespesa"}, allowSetters = true)
	private List<ContaPagar> contasPagar;

	// Construtor
	public Despesa() {
		this.contasPagar = new ArrayList<>();
	}
		
}
