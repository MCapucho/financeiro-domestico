package br.com.systemmcr.sysfinan.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "conta_receber")
public class ContaReceber {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private Long id;
	
	@NotNull
	@Column(name = "data_recebimento")
	private LocalDate dataRecebimento;
	
	@ManyToOne
	@JoinColumn(name = "codigo_receita")
	private Receita tipoReceita;
	
	@Column(name = "numero_documento")
	private String numeroDocumento;
	
	@NotNull
	private BigDecimal valor;
	
	@Size(max = 200)
	private String observacao;
	
	@Enumerated(EnumType.STRING)
	private StatusTitulo statusTitulo;
	
}
