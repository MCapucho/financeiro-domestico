package br.com.systemmcr.sysfinan.util;

public class MessageValidation {

	public static final String MSG_RECEITA_EM_USO = "Receita de código %d não pode ser removida, pois está em uso";
	public static final String MSG_RECEITA_EXISTENTE = "Receita já existente";
	public static final String MSG_RECEITA_PROIBIDO_ALTERACAO = "Proibido atualizar, o elemento tem um lançamento";
	
	public static final String MSG_DESPESA_EM_USO = "Despesa de código %d não pode ser removida, pois está em uso";
	public static final String MSG_DESPESA_EXISTENTE = "Despesa já existente";
	public static final String MSG_DESPESA_PROIBIDO_ALTERACAO = "Proibido atualizar, o elemento tem um lançamento";
	
	public static final String MSG_USUARIO_ADM_PROIBIDO_EXCLUIR = "O administrador não pode ser excluído";
	public static final String MSG_USUARIO_EMAIL_EXISTENTE = "E-mail já existente";
	public static final String MSG_USUARIO_PERMISSAO_EXISTENTE = "Permissão já existente";
			
}
