package br.com.systemmcr.sysfinan.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.model.Usuario;
import br.com.systemmcr.sysfinan.repository.UsuarioRepository;
import br.com.systemmcr.sysfinan.service.UsuarioService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/usuarios")
public class UsuarioResource {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_USUARIO_CRIAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Usuario> create(@Valid @RequestBody Usuario usuario, HttpServletResponse response) {
		Usuario usuarioNovo = usuarioService.create(usuario);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioNovo);
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_USUARIO_FILTRAR') and #oauth2.hasScope('read')")
	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}
	
	@GetMapping("/page")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_FILTRAR') and #oauth2.hasScope('read')")
	public Page<Usuario> findByPage(FilterPageable filterPageable) {
		return usuarioService.findByPage(filterPageable);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_FILTRAR') and #oauth2.hasScope('read')")
	public ResponseEntity<Usuario> findById(@PathVariable Long id) {
		Usuario usuario = usuarioService.findById(id);
		
		return usuario != null ? ResponseEntity.ok(usuario) : ResponseEntity.notFound().build();
	}
	
	@GetMapping("/excetoAdm")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_FILTRAR') and #oauth2.hasScope('read')")
	public List<Usuario> findByUsuarioExcetoAdm() {
		return usuarioRepository.findByUsuarioExcetoAdm();
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_USUARIO_DELETAR') and #oauth2.hasScope('write')")
	public void delete(@PathVariable Long id) {
		usuarioService.deleteById(id);
	}
		
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Usuario> update(@PathVariable Long id, @Valid @RequestBody Usuario usuario) {
		Usuario usuarioSalvo = usuarioService.update(id, usuario);
		
		return ResponseEntity.ok(usuarioSalvo);
	}
	
	@PutMapping("/senha/{id}")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Usuario> updatePassword(@PathVariable Long id, @Valid @RequestBody Usuario usuario) {
		Usuario usuarioSalvo = usuarioService.updatePassword(id, usuario);
		
		return ResponseEntity.ok(usuarioSalvo);
	}
	
	@PutMapping("/acesso/{id}")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Usuario> updateAcess(@PathVariable Long id, @Valid @RequestBody Usuario usuario) {
		Usuario usuarioSalvo = usuarioService.updateAcess(id, usuario);
		
		return ResponseEntity.ok(usuarioSalvo);
	}
	
}
