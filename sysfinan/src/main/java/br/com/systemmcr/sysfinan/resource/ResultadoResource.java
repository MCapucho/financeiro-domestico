package br.com.systemmcr.sysfinan.resource;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.dto.ContasDTO;
import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.service.ContaPagarService;
import br.com.systemmcr.sysfinan.service.ContaReceberService;
import br.com.systemmcr.sysfinan.service.ResultadoService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/resultados")
public class ResultadoResource {
	
	@Autowired
	private ResultadoService resultadoService;
	
	@Autowired
	private ContaPagarService contaPagarService;
	
	@Autowired
	private ContaReceberService contaReceberService;
	
	@GetMapping("/contaReceber")
	public Page<ContaReceber> findByDataPagamentoBetweenCR(@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate dataInicial, 
			                                               @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate dataFinal,
			                                               FilterPageable filterPageable) {
		return contaReceberService.findByDataPagamentoBetweenCR(dataInicial, dataFinal, filterPageable);
	}	
	
	@GetMapping("/contaPagar")
	public Page<ContaPagar> findByDataPagamentoBetweenCP(@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate dataInicial, 
			                                             @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate dataFinal,
			                                             FilterPageable filterPageable) {
		return contaPagarService.findByDataPagamentoBetweenCP(dataInicial, dataFinal, filterPageable);
	}
	
	@GetMapping("/final")
	public ContasDTO contasFiltradas(@RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate dataInicial, 
            						 @RequestParam @DateTimeFormat(iso = ISO.DATE) LocalDate dataFinal) {
		return resultadoService.contasFiltradas(dataInicial, dataFinal);
	}
	
}
