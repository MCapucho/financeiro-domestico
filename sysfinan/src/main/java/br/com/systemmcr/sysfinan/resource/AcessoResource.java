package br.com.systemmcr.sysfinan.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.dto.UsuarioPermissaoDTO;
import br.com.systemmcr.sysfinan.service.UsuarioService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/acessos")
public class AcessoResource {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/page")
	public Page<UsuarioPermissaoDTO> findUsuarioPermissao(FilterPageable filterPageable) {
		return usuarioService.findByPagePermissao(filterPageable);
	}
	
}
