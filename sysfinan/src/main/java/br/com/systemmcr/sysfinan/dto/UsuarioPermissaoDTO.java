package br.com.systemmcr.sysfinan.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioPermissaoDTO {

	public String nome;
	public String descricao;
	
	// Construtor
	public UsuarioPermissaoDTO(String nome, String descricao) {
		this.nome = nome;
		this.descricao = descricao;
	}
	
	
}
