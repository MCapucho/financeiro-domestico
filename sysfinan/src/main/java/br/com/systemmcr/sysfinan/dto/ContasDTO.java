package br.com.systemmcr.sysfinan.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.model.ContaReceber;
import lombok.Data;

@Data
public class ContasDTO {

	private List<ContaPagar> contasDespesasQuitadas;
	private List<ContaPagar> contasDespesasAbertas;
	private List<ContaReceber> contasReceitasQuitadas;
	private List<ContaReceber> contasReceitasAbertas;
	
	// Construtor
	public ContasDTO() {
		this.contasDespesasAbertas = new ArrayList<>();
		this.contasDespesasQuitadas = new ArrayList<>();
		this.contasReceitasAbertas = new ArrayList<>();
		this.contasReceitasQuitadas = new ArrayList<>();
	}
	
}
