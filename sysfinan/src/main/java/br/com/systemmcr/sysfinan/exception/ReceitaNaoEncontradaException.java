package br.com.systemmcr.sysfinan.exception;

public class ReceitaNaoEncontradaException extends EntidadeNaoEncontradaException {

	private static final long serialVersionUID = 1L;

	public ReceitaNaoEncontradaException(String mensagem) {
		super(mensagem);
	}
	
	public ReceitaNaoEncontradaException(Long id) {
		this(String.format("Não existe uma receita com código %d", id));
	}
	
}
