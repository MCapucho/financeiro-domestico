package br.com.systemmcr.sysfinan.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.model.ContaPagar;
import br.com.systemmcr.sysfinan.service.ContaPagarService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/contasPagar")
public class ContaPagarResource {

	@Autowired
	private ContaPagarService contaPagarService;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTAPAGAR_CRIAR') and #oauth2.hasScope('write')")
	public ResponseEntity<ContaPagar> create(@Valid @RequestBody ContaPagar contaPagar, HttpServletResponse response) {
		ContaPagar contaPagarNova = contaPagarService.create(contaPagar);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(contaPagarNova);
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTAPAGAR_FILTRAR') and #oauth2.hasScope('read')")
	public List<ContaPagar> findAll() {
		return contaPagarService.findAll();
	}
	
	@GetMapping("/page")
	@PreAuthorize("hasAuthority('ROLE_CONTAPAGAR_FILTRAR') and #oauth2.hasScope('read')")
	public Page<ContaPagar> findByPage(FilterPageable filterPageable) {
		return contaPagarService.findByPage(filterPageable);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CONTAPAGAR_FILTRAR') and #oauth2.hasScope('read')")
	public ResponseEntity<ContaPagar> findById(@PathVariable Long id) {
		ContaPagar contaPagar = contaPagarService.findById(id);
		
		return contaPagar != null ? ResponseEntity.ok(contaPagar) : ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_CONTAPAGAR_DELETAR') and #oauth2.hasScope('write')")
	public void deleteById(@PathVariable Long id) {
		contaPagarService.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CONTAPAGAR_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<ContaPagar> update(@PathVariable Long id, @Valid @RequestBody ContaPagar contaPagar) {
		ContaPagar contaPagarSalva = contaPagarService.update(id, contaPagar);
		
		return ResponseEntity.ok(contaPagarSalva);
	}
	
}
