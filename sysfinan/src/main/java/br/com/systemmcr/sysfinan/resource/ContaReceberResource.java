package br.com.systemmcr.sysfinan.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.service.ContaReceberService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/contasReceber")
public class ContaReceberResource {
	
	@Autowired
	private ContaReceberService contaReceberService;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CRIAR') and #oauth2.hasScope('write')")
	public ResponseEntity<ContaReceber> create(@Valid @RequestBody ContaReceber contaReceber, HttpServletResponse response) {		
		ContaReceber contaReceberNova = contaReceberService.create(contaReceber);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(contaReceberNova);
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_FILTRAR') and #oauth2.hasScope('read')")
	public List<ContaReceber> findAll() {
		return contaReceberService.findAll();
	}
	
	@GetMapping("/page")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_FILTRAR') and #oauth2.hasScope('read')")
	public Page<ContaReceber> findByPage(FilterPageable filterPageable) {
		return contaReceberService.findByPage(filterPageable);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_FILTRAR') and #oauth2.hasScope('read')")
	public ResponseEntity<ContaReceber> findById(@PathVariable Long id) {
		ContaReceber contaReceber = contaReceberService.findById(id);
		
		return contaReceber != null ? ResponseEntity.ok(contaReceber) : ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_DELETAR') and #oauth2.hasScope('write')")
	public void deleteById(@PathVariable Long id) {
		contaReceberService.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<ContaReceber> update(@PathVariable Long id, @Valid @RequestBody ContaReceber contaReceber) {
		ContaReceber contaReceberSalva = contaReceberService.update(id, contaReceber);
		
		return ResponseEntity.ok(contaReceberSalva);
	}
		
}
