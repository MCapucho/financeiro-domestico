package br.com.systemmcr.sysfinan.exception;

public class UsuarioPadraoException extends NegocioException {
	
	private static final long serialVersionUID = 1L;

	public UsuarioPadraoException(String mensagem) {
		super(mensagem);
	}
	
}
