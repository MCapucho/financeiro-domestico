package br.com.systemmcr.sysfinan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.systemmcr.sysfinan.model.Despesa;

@Repository
public interface DespesaRepository extends JpaRepository<Despesa, Long> {

	Despesa findByDescricao(String descricao);

	List<Despesa> findByStatusTrue();
	
}
