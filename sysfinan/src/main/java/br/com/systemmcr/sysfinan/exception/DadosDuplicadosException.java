package br.com.systemmcr.sysfinan.exception;

public class DadosDuplicadosException extends NegocioException {
	
	private static final long serialVersionUID = 1L;

	public DadosDuplicadosException(String mensagem) {
		super(mensagem);
	}
	
}
