package br.com.systemmcr.sysfinan.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.systemmcr.sysfinan.dto.UsuarioPermissaoDTO;
import br.com.systemmcr.sysfinan.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Optional<Usuario> findByEmail(String email);
	
	Usuario findByUsuarioExistente(String email);
	
	List<Usuario> findByUsuarioExcetoAdm();
	
	@Query(value = "SELECT new br.com.systemmcr.sysfinan.dto.UsuarioPermissaoDTO(u.nome, p.descricao) FROM Usuario u JOIN u.permissoes p ") 
	Page<UsuarioPermissaoDTO> findByUsuarioPermissao(Pageable pageable);
}
