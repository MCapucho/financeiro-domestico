package br.com.systemmcr.sysfinan.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import br.com.systemmcr.sysfinan.exception.ContaReceberNaoEncontradaException;
import br.com.systemmcr.sysfinan.mail.Mailer;
import br.com.systemmcr.sysfinan.model.ContaReceber;
import br.com.systemmcr.sysfinan.model.StatusTitulo;
import br.com.systemmcr.sysfinan.model.Usuario;
import br.com.systemmcr.sysfinan.repository.ContaReceberRepository;
import br.com.systemmcr.sysfinan.repository.UsuarioRepository;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@Service
public class ContaReceberService {
	
	private static final Logger logger = LoggerFactory.getLogger(ContaPagarService.class);
	
	@Autowired
	private ContaReceberRepository contaReceberRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private Mailer mailer;
		
	public ContaReceber create(ContaReceber contaReceber) {
		contaReceber.setStatusTitulo(StatusTitulo.ABERTO);
		
		return contaReceberRepository.save(contaReceber);
	}
	
	public List<ContaReceber> findAll() {
		return contaReceberRepository.findAll();
	}
	
	public Page<ContaReceber> findByPage(FilterPageable filterPageable) {
		return contaReceberRepository.findAll(filterPageable.listByPage());
	}
	
	public ContaReceber findById(Long id) {
		return contaReceberRepository.findById(id).orElseThrow(() -> new ContaReceberNaoEncontradaException(id));
	}
	
	public void deleteById(Long id) {
		try {
			contaReceberRepository.deleteById(id);
			
		} catch (EmptyResultDataAccessException e) {
			throw new ContaReceberNaoEncontradaException(id);
		}
	}	
		
	public ContaReceber update(Long id, ContaReceber contaReceber) {
		ContaReceber contaReceberSalva = findById(id);
		
		BeanUtils.copyProperties(contaReceber, contaReceberSalva, "id");
		
		return contaReceberRepository.save(contaReceberSalva);
	}
	
	public Double monthlyTotalizerCR() {
		List<ContaReceber> contas = contaReceberRepository.monthlyTotalizerCR(LocalDate.now().getMonth().getValue());
		
		Double total = contas.stream()
							 .mapToDouble(obj -> obj.getValor().doubleValue())
							 .sum();
		
		return total;
	}

	public Page<ContaReceber> findByDataPagamentoBetweenCR(LocalDate dataInicial, LocalDate dataFinal, FilterPageable filterPageable) {
		Page<ContaReceber> resultado = contaReceberRepository.findByDataRecebimentoBetween(dataInicial, 
				                                                                           dataFinal,
				                                                                           filterPageable.listByPage());
		
		return resultado;
	}
	
	@Scheduled(cron = "0 0 9 * * *")
	public void avisarSobreContaReceberEmAberto() {
		if ( logger.isDebugEnabled()) {
			logger.debug("Preparando envio de e-mails de aviso de contas à receber");
		}
		
		List<ContaReceber> abertos = contaReceberRepository.findByDataRecebimentoLessThanEqualAndStatusTitulo(LocalDate.now(), StatusTitulo.ABERTO);
		
		if (abertos.isEmpty()) {
			logger.info("Contas à receber em dia!");
			
			return;
		}
		
		logger.info("Existem {} contas à receber em abertos", abertos.size());
		
		List<Usuario> destinatarios = usuarioRepository.findAll();
		
		if (destinatarios.isEmpty()) {
			logger.warn("Existem contas à receber, porém o sistema não encontrou destinatários");
			
			return;
		}
		
		mailer.avisarSobreContaReceberEmAberto(abertos, destinatarios);
		
		logger.info("Envio de e-mail de aviso concluído!");
	}
		
}
