package br.com.systemmcr.sysfinan.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.systemmcr.sysfinan.model.Despesa;
import br.com.systemmcr.sysfinan.repository.DespesaRepository;
import br.com.systemmcr.sysfinan.service.DespesaService;
import br.com.systemmcr.sysfinan.util.FilterPageable;

@RestController
@RequestMapping("/despesas")
public class DespesaResource {
	
	@Autowired
	private DespesaRepository despesaRepository;
	
	@Autowired
	private DespesaService despesaService;
		
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_DESPESA_CRIAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Despesa> create(@Valid @RequestBody Despesa despesa, HttpServletResponse response) {
		Despesa despesaNova = despesaService.create(despesa);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(despesaNova);
	}
		
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_DESPESA_FILTRAR') and #oauth2.hasScope('read')")
	public List<Despesa> findAll() {
		return despesaRepository.findAll();
	}
	
	@GetMapping("/page")
	@PreAuthorize("hasAuthority('ROLE_DESPESA_FILTRAR') and #oauth2.hasScope('read')")
    public Page<Despesa> findByPage(FilterPageable filterPageable) {
        return despesaService.findByPage(filterPageable);
    }
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_DESPESA_FILTRAR') and #oauth2.hasScope('read')")
	public ResponseEntity<Despesa> findById(@PathVariable Long id) {
		Despesa despesa = despesaService.findById(id);
		
		return despesa != null ? ResponseEntity.ok(despesa) : ResponseEntity.notFound().build(); 
	}
	
	@GetMapping("/ativo")
	@PreAuthorize("hasAuthority('ROLE_DESPESA_FILTRAR') and #oauth2.hasScope('read')")
	public List<Despesa> findByStatusAtivo() {
		return despesaRepository.findByStatusTrue();
	}
		
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@PreAuthorize("hasAuthority('ROLE_DESPESA_DELETAR') and #oauth2.hasScope('write')")
	public void delete(@PathVariable Long id) {
		despesaService.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_DESPESA_ATUALIZAR') and #oauth2.hasScope('write')")
	public ResponseEntity<Despesa> update(@PathVariable Long id, @Valid @RequestBody Despesa despesa) {
		Despesa despesaSalva = despesaService.update(id, despesa);
		
		return ResponseEntity.ok(despesaSalva);
	}
	
}
