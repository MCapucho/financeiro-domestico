package br.com.systemmcr.sysfinan.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "receita")
public class Receita {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(min = 3, max = 100)
	private String descricao;
	
	@NotNull
	private Boolean status;
	
	@OneToMany(mappedBy = "tipoReceita", fetch = FetchType.LAZY)
	@JsonIgnoreProperties(value = {"tipoReceita"}, allowSetters = true)
	private List<ContaReceber> contasReceber;

	// Construtor
	public Receita() {
		this.contasReceber = new ArrayList<>();
	}
		
}
