import { HomePage } from './pageObject/home.po';
import { LoginPage } from './pageObject/login.po';

describe('Testando tela de login', () => {
    let loginPage: LoginPage;
    let homePage: HomePage;

    beforeEach(() => {
        loginPage = new LoginPage();
        homePage = new HomePage();
    });

    it('Deve acessar o sistema', () => {
        loginPage.acessarSistema();
    });

    it('Deve verificar a url', () => {
        expect(loginPage.verificarUrl()).toBe('http://localhost:4200/login');
    });

    it('Deve fazer o login', () => {
        expect(loginPage.pegarInputLogin('usuario', 'admin@sysfinan.com'));
        expect(loginPage.pegarInputLogin('senha', 'admin'));
        expect(loginPage.pegarBotaoLogin().click());
    });

    it('Deve ir para a tela de home', () => {
        homePage.navegarParaHome();
    });

});
