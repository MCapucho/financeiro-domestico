import { browser, element, by } from 'protractor';

export class LoginPage {
    verificarUrl() {
        return browser.getCurrentUrl();
    }

    acessarSistema() {
        return browser.get('/');
    }

    pegarInputLogin(name, valor) {
        return element(by.css(`input[name="${name}"]`)).sendKeys(`${valor}`);
    }

    pegarBotaoLogin() {
        return element(by.buttonText('Entrar'));
    }

}
