export const environment = {
  production: true,
  apiUrl: 'https://sysfinan-api-mcr.herokuapp.com',

  // tokenWhitelistedDomains: [ new RegExp('sysfinan-api-mcr.herokuapp.com') ],
  // tokenBlacklistedRoutes: [ new RegExp('\/oauth\/token') ]
};
