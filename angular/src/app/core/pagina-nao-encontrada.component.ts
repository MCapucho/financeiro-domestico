import { Component, OnInit } from '@angular/core';

@Component({
  template: `
    <div class="mcr-container">
      <h1 class="text-center">Página não encontrada</h1>

      <div class="text-center">
      <img src="./../assets/img/acesso_negado.jpg" alt="Acesso Negado">
    </div>
    <div>
  `,
  styles: []
})

export class PaginaNaoEncontradaComponent {

}
