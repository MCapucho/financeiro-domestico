import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule, MatNativeDateModule, MatTableModule, MatPaginatorModule,
  MatToolbarModule, MatIconModule, MatDialogModule, MatButtonModule, MatSelectModule,
  MatSortModule, MatSnackBarModule, MatRadioModule, MatCardModule, MatCheckboxModule,
  MatTabsModule, MatTooltipModule, MatDividerModule, MatAutocompleteModule,
  MatProgressSpinnerModule, MatListModule, MatChipsModule, MatExpansionModule, MatMenuModule
} from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { ErrorHandlerService } from './error-handler.service';
import { LoginService } from './../login/shared/login.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NaoAutorizadoComponent } from './nao-autorizado.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { PaginaNaoEncontradaComponent } from './pagina-nao-encontrada.component';
import { LoginHttp } from '../login/shared/login-http';

import { ButtonComponent } from './../shared/componentes-globais/button/button.component';

export const customCurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: ',',
  precision: 2,
  prefix: 'R$ ',
  suffix: '',
  thousands: '.',
  nullable: true
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatChipsModule,
    MatExpansionModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,

    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
  ],

  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatRadioModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatTabsModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatDividerModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatChipsModule,
    MatExpansionModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,

    ButtonComponent,
  ],

  declarations: [
    NaoAutorizadoComponent,
    PaginaNaoEncontradaComponent,
    ButtonComponent,
  ],

  providers: [
    ErrorHandlerService,
    LoginService,
    JwtHelperService,
    LoginHttp
  ],

  entryComponents: [
  ]
})

export class CoreModule {}
