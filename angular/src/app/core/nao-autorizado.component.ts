import { Component, OnInit } from '@angular/core';

@Component({
  template: `
  <div class="mcr-container">
    <h1 class="text-center">Acesso negado!</h1>

    <div class="text-center">
      <img src="./../assets/img/acesso_negado.jpg" alt="Acesso Negado">
    </div>
  </div>
  `,
})

export class NaoAutorizadoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
