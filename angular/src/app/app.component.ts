import { LoaderService } from './shared/interceptor/loader/loader.service';
import { Component, ChangeDetectorRef, OnInit, AfterContentInit } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd, NavigationEnd } from '@angular/router';

import { LoginService } from './login/shared/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentInit {

  constructor(private router: Router,
              private loaderService: LoaderService,
              private cdRef: ChangeDetectorRef) {

  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    this.reloadIfSameUrl();
    this.prepareNavigation();
  }

  exibindoTemplate() {
    return this.router.url !== '/login';
  }

  prepareNavigation(): any {
    this.router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart) {
        this.loaderService.show(event.route.path);
      } else if (event instanceof RouteConfigLoadEnd) {
        this.loaderService.hide(event.route.path);
      } else if (event instanceof NavigationEnd) {
        this.router.navigated = false;
      }
      this.cdRef.detectChanges();
    });
  }

  reloadIfSameUrl(): any {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
  }
}
