import { Receita } from './../../../cadastro/receita/shared/receita.model';

export class ContaReceber {
  id?: number;
  dataRecebimento?: string;
  tipoReceita?: Receita;
  numeroDocumento?: string;
  valor?: number;
  observacao?: string;
  statusTitulo?: string;
}
