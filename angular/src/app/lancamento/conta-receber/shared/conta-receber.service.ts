import { Paginator } from './../../../shared/model/paginator.model';
import { Injectable } from '@angular/core';

import { ContaReceber } from './conta-receber.model';

import { Observable, BehaviorSubject } from 'rxjs';
import { LoginHttp } from 'src/app/login/shared/login-http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContaReceberService {

  dataChange: BehaviorSubject<ContaReceber[]> = new BehaviorSubject<ContaReceber[]>([]);

  contasReceberURL: string;

  constructor(private http: LoginHttp) {
    this.contasReceberURL = `${environment.apiUrl}/contasReceber`;
  }

  create(contaReceber: ContaReceber): any {
    return this.http.post(this.contasReceberURL, contaReceber);
  }

  update(contaReceber: ContaReceber): any {
    return this.http.put(`${this.contasReceberURL}/${contaReceber.id}`, contaReceber);
  }

  delete(contaReceber: ContaReceber): any {
    return this.http.delete(`${this.contasReceberURL}/${contaReceber.id}`);
  }

  findByPage(direction, orderBy, linesPerPage, page): Observable<Paginator<ContaReceber>> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<ContaReceber>>(`${this.contasReceberURL}/page?direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }
}
