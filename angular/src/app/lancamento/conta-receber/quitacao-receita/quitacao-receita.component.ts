import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

import { ContaReceber } from './../shared/conta-receber.model';

@Component({
  selector: 'app-quitacao-receita',
  templateUrl: './quitacao-receita.component.html',
  styleUrls: ['./quitacao-receita.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class QuitacaoReceitaComponent implements OnInit {

  contaReceber: ContaReceber;

  closeDialog: boolean;

  constructor() { }

  ngOnInit() {
    this.dialogClose();
  }

  dialogClose() {
    this.closeDialog = true;
  }

  quitarTituloReceita() {
    this.contaReceber.statusTitulo = 'QUITADO';
    this.dialogClose();
  }

}
