import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuitacaoReceitaComponent } from './quitacao-receita.component';

describe('QuitacaoReceitaComponent', () => {
  let component: QuitacaoReceitaComponent;
  let fixture: ComponentFixture<QuitacaoReceitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuitacaoReceitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuitacaoReceitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
