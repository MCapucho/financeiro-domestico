import { Component, OnInit, ViewChild, LOCALE_ID } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS,
         MatTableDataSource, MatPaginator, MatSnackBar, MatDialog } from '@angular/material';

import * as _ from 'lodash';
import { tap } from 'rxjs/operators';

import { registerLocaleData } from '@angular/common';
import localept from '@angular/common/locales/pt';

registerLocaleData(localept, 'pt');

import { ReceitaService } from './../../cadastro/receita/shared/receita.service';
import { ContaReceberService } from './shared/conta-receber.service';

import { ContaReceber } from './shared/conta-receber.model';
import { Receita } from './../../cadastro/receita/shared/receita.model';

import { QuitacaoReceitaComponent } from './quitacao-receita/quitacao-receita.component';

import { ConfirmacaoComponent } from '../../shared/componentes-globais/confirmacao/confirmacao.component';
import { LANÇAR_CONTA_RECEBER, DELETAR_CONTA_RECEBER, EDITAR_CONTA_RECEBER,
         CONFIRMACAO_DELETE, QUITAR_TITULO } from './../../shared/texto-consts/texto-consts';
import { PAGE_SIZES } from './../../shared/tabela/tabela';


@Component({
  selector: 'app-conta-receber',
  templateUrl: './conta-receber.component.html',
  styleUrls: ['./conta-receber.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ContaReceberComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'dataRecebimento',
    'tipoReceita',
    'numeroDocumento',
    'valor',
    'observacao',
    'editar',
    'excluir',
    'quitarTitulo'
  ];

  dataSource: MatTableDataSource<Receita>;

  contaReceber: ContaReceber;

  receitas: Receita[];

  editing: boolean;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  constructor(private contaReceberService: ContaReceberService,
              private receitaService: ReceitaService,
              public snackBar: MatSnackBar,
              private adapter: DateAdapter<any>,
              private dialog: MatDialog) { }

  brasileiro() {
    this.adapter.setLocale('br');
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<ContaReceber>();
    this.filtrarReceita();
    this.checarContaReceber();
    this.iniciarPaginacao();
    this.carregarTabela();
  }

  checarContaReceber() {
    if (this.contaReceber === undefined) {
      this.resetar();
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  resetar() {
    this.contaReceber = {
      dataRecebimento: null,
      tipoReceita: {},
      valor: null,
      observacao: null
    };
    this.receitas = [];
    this.filtrarReceita();
  }

  resetarEditando() {
    this.contaReceber = {
      id: this.contaReceber.id,
      dataRecebimento: null,
      tipoReceita: {},
      valor: null,
      observacao: null
    };
    this.receitas = [];
    this.filtrarReceita();
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.carregarTabela();
      })
    ).subscribe();
  }

  popularTabela(dataSource: ContaReceber[]) {
    this.dataSource.data = dataSource;
  }

  carregarTabela() {
    this.contaReceberService.findByPage(
      this.direction = 'ASC',
      this.orderBy = 'dataRecebimento',
      this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
      this.paginator.pageIndex ? this.paginator.pageIndex : 0
    ).subscribe(res => {
      this.popularTabela(res.content);
      this.paginatorLength = res.totalElements;
    });
  }

  filtrarReceita() {
    this.receitaService.findByStatusTrue().subscribe(res => {
        this.receitas = res;
    });
  }

  atribuirReceita(ev) {
    if (ev.source.value !== null && ev.source.value !== undefined) {
      this.contaReceber.tipoReceita = this.receitas.find( element => {
        return element.id === ev.source.value;
      });
    } else {
      this.contaReceber.tipoReceita = { id: null };
    }
  }

  criar() {
    this.contaReceberService.create(this.contaReceber).subscribe(result => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(LANÇAR_CONTA_RECEBER, 'X', {
        duration: 3000
      });
    }, error => {
      this.snackBar.open(error.error.message, 'X', {
        duration: 3000
      });
    });
  }

  atualizar(row: Receita) {
    const oldRegistry = _.cloneDeep(row);

    this.contaReceber = oldRegistry;

    this.checarContaReceber();
  }

  editar() {
    this.contaReceberService.update(this.contaReceber).subscribe(() => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(EDITAR_CONTA_RECEBER, 'X', {
        duration: 3000
      });
    }, error => {
      this.snackBar.open(error.error.message, 'X', {
        duration: 3000
      });
    });
  }

  excluir(row: ContaReceber) {
    this.contaReceberService.delete(row).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(DELETAR_CONTA_RECEBER, 'X', {
        duration: 2000
      });
    });
  }

  deletarDialogo(contaReceber: ContaReceber) {
    const dialogRef = this.dialog.open(ConfirmacaoComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_DELETE;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(contaReceber);
      }
    });
  }

  quitarTituloReceita(row: ContaReceber) {
    const dialogRef = this.dialog.open(QuitacaoReceitaComponent, {
      disableClose: true,
      width: '60%'
    });

    const oldRegistry = _.cloneDeep(row);

    dialogRef.componentInstance.contaReceber = oldRegistry;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contaReceberService.update(dialogRef.componentInstance.contaReceber).subscribe(() => {
          this.carregarTabela();
          this.snackBar.open(QUITAR_TITULO, 'X', {
            duration: 2000
          });
        });
      }
    });
  }

  cancelar() {
    this.contaReceber = undefined;
    this.checarContaReceber();
    this.resetar();
  }
}
