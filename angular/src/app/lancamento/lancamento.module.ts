import { NgModule } from '@angular/core';

import { CoreModule } from './../core/core.module';
import { NgxCurrencyModule } from 'ngx-currency';
import { LancamentoRoutingModule } from './lancamento-routing.module';

import { ContaPagarComponent } from './conta-pagar/conta-pagar.component';
import { ContaReceberComponent } from './conta-receber/conta-receber.component';
import { QuitacaoReceitaComponent } from './conta-receber/quitacao-receita/quitacao-receita.component';
import { QuitacaoDespesaComponent } from './conta-pagar/quitacao-despesa/quitacao-despesa.component';

export const customCurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  allowZero: true,
  decimal: ',',
  precision: 2,
  prefix: 'R$ ',
  suffix: '',
  thousands: '.',
  nullable: true
};

@NgModule({
  declarations: [
    ContaReceberComponent,
    ContaPagarComponent,
    QuitacaoDespesaComponent,
    QuitacaoReceitaComponent,
  ],

  imports: [
    CoreModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),

    LancamentoRoutingModule
  ],

  exports: [
    ContaReceberComponent,
    ContaPagarComponent,
    QuitacaoDespesaComponent,
    QuitacaoReceitaComponent,
  ],

  entryComponents: [
    QuitacaoDespesaComponent,
    QuitacaoReceitaComponent,
  ]
})

export class LancamentoModule {}
