import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from './../login/auth.guard';

import { ContaPagarComponent } from './conta-pagar/conta-pagar.component';
import { ContaReceberComponent } from './conta-receber/conta-receber.component';

const ROUTES: Routes = [
    {
        path: 'conta-receber',
        component: ContaReceberComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_CONTARECEBER_FILTRAR']}
    },
    {
        path: 'conta-pagar',
        component: ContaPagarComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_CONTAPAGAR_FILTRAR']}
    },
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class LancamentoRoutingModule { }
