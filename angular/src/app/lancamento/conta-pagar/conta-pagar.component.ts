import { Component, OnInit, ViewChild, LOCALE_ID } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatTableDataSource,
         MatPaginator, MatSnackBar, MatDialog } from '@angular/material';

import * as _ from 'lodash';
import { tap } from 'rxjs/operators';

import { registerLocaleData } from '@angular/common';
import localept from '@angular/common/locales/pt';

registerLocaleData(localept, 'pt');

import { DespesaService } from './../../cadastro/despesa/shared/despesa.service';
import { ContaPagarService } from './shared/conta-pagar.service';

import { ContaPagar } from './shared/conta-pagar.model';
import { Despesa } from './../../cadastro/despesa/shared/despesa.model';

import { QuitacaoDespesaComponent } from './quitacao-despesa/quitacao-despesa.component';

import { ConfirmacaoComponent } from '../../shared/componentes-globais/confirmacao/confirmacao.component';
import { LANÇAR_CONTA_PAGAR, DELETAR_CONTA_PAGAR, EDITAR_CONTA_PAGAR,
         CONFIRMACAO_DELETE, QUITAR_TITULO} from './../../shared/texto-consts/texto-consts';
import { PAGE_SIZES } from './../../shared/tabela/tabela';

@Component({
  selector: 'app-conta-pagar',
  templateUrl: './conta-pagar.component.html',
  styleUrls: ['./conta-pagar.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ContaPagarComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'dataPagamento',
    'tipoDespesa',
    'numeroDocumento',
    'valor',
    'observacao',
    'editar',
    'excluir',
    'quitarTitulo'
  ];

  dataSource: MatTableDataSource<Despesa>;

  contaPagar: ContaPagar;

  despesas: Despesa[];

  editing: boolean;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  constructor(private contaPagarService: ContaPagarService,
              private despesaServie: DespesaService,
              public snackBar: MatSnackBar,
              private adapter: DateAdapter<any>,
              private dialog: MatDialog) { }

  brasileiro() {
    this.adapter.setLocale('br');
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<ContaPagar>();
    this.filtrarDespesa();
    this.checarContaPagar();
    this.iniciarPaginacao();
    this.carregarTabela();
  }

  checarContaPagar() {
    if (this.contaPagar === undefined) {
      this.resetar();
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  resetar() {
    this.contaPagar = {
      dataPagamento: null,
      tipoDespesa: {},
      valor: null,
      observacao: null
    };
    this.despesas = [];
    this.filtrarDespesa();
  }

  resetarEditando() {
    this.contaPagar = {
      id: this.contaPagar.id,
      dataPagamento: null,
      tipoDespesa: {},
      valor: null,
      observacao: null
    };
    this.despesas = [];
    this.filtrarDespesa();
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.carregarTabela();
      })
    ).subscribe();
  }

  popularTabela(dataSource: ContaPagar[]) {
    this.dataSource.data = dataSource;
  }

  carregarTabela() {
    this.contaPagarService.findByPage(
      this.direction = 'ASC',
      this.orderBy = 'dataPagamento',
      this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
      this.paginator.pageIndex ? this.paginator.pageIndex : 0
    ).subscribe(res => {
      this.popularTabela(res.content);
      this.paginatorLength = res.totalElements;
    });
  }

  filtrarDespesa() {
    this.despesaServie.findByStatusTrue().subscribe(res => {
        this.despesas = res;
    });
  }

  atribuirDespesa(ev) {
    if (ev.source.value !== null && ev.source.value !== undefined) {
      this.contaPagar.tipoDespesa = this.despesas.find( element => {
        return element.id === ev.source.value;
      });
    } else {
      this.contaPagar.tipoDespesa = { id: null };
    }
  }

  criar() {
    this.contaPagarService.create(this.contaPagar).subscribe(result => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(LANÇAR_CONTA_PAGAR, 'X', {
        duration: 3000
      });
    }, error => {
      this.snackBar.open(error.error.message, 'X', {
        duration: 3000
      });
    });
  }

  atualizar(row: Despesa) {
    const oldRegistry = _.cloneDeep(row);

    this.contaPagar = oldRegistry;

    this.checarContaPagar();
  }

  editar() {
    this.contaPagarService.update(this.contaPagar).subscribe(() => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(EDITAR_CONTA_PAGAR, 'X', {
        duration: 3000
      });
    }, error => {
      this.snackBar.open(error.error.message, 'X', {
        duration: 3000
      });
    });
  }

  excluir(row: ContaPagar) {
    this.contaPagarService.delete(row).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(DELETAR_CONTA_PAGAR, 'X', {
        duration: 2000
      });
    });
  }

  deletarDialogo(contaPagar: ContaPagar) {
    const dialogRef = this.dialog.open(ConfirmacaoComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_DELETE;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(contaPagar);
      }
    });
  }

  quitarTituloDespesa(row: ContaPagar) {
    const dialogRef = this.dialog.open(QuitacaoDespesaComponent, {
      disableClose: true,
      width: '60%'
    });

    const oldRegistry = _.cloneDeep(row);

    dialogRef.componentInstance.contaPagar = oldRegistry;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contaPagarService.update(dialogRef.componentInstance.contaPagar).subscribe(() => {
          this.carregarTabela();
          this.snackBar.open(QUITAR_TITULO, 'X', {
            duration: 2000
          });
        });
      }
    });
  }

  cancelar() {
    this.contaPagar = undefined;
    this.checarContaPagar();
    this.resetar();
  }

}
