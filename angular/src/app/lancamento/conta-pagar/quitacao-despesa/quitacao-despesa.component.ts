import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

import { ContaPagar } from '../shared/conta-pagar.model';

@Component({
  selector: 'app-quitacao-despesa',
  templateUrl: './quitacao-despesa.component.html',
  styleUrls: ['./quitacao-despesa.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class QuitacaoDespesaComponent implements OnInit {

  contaPagar: ContaPagar;

  closeDialog: boolean;

  constructor() { }

  ngOnInit() {
    this.dialogClose();
  }

  dialogClose() {
    this.closeDialog = true;
  }

  quitarTituloDespesa() {
    this.contaPagar.statusTitulo = 'QUITADO';
    this.dialogClose();
  }

}
