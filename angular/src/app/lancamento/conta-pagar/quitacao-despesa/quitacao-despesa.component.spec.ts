import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuitacaoDespesaComponent } from './quitacao-despesa.component';

describe('QuitacaoDespesaComponent', () => {
  let component: QuitacaoDespesaComponent;
  let fixture: ComponentFixture<QuitacaoDespesaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuitacaoDespesaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuitacaoDespesaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
