import { Paginator } from './../../../shared/model/paginator.model';
import { Injectable } from '@angular/core';

import { ContaPagar } from './conta-pagar.model';

import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginHttp } from 'src/app/login/shared/login-http';

@Injectable({
  providedIn: 'root'
})

export class ContaPagarService {

  dataChange: BehaviorSubject<ContaPagar[]> = new BehaviorSubject<ContaPagar[]>([]);

  contasPagarURL: string;

  constructor(private http: LoginHttp) {
    this.contasPagarURL = `${environment.apiUrl}/contasPagar`;
  }

  create(contaPagar: ContaPagar): any {
    return this.http.post(this.contasPagarURL, contaPagar);
  }

  update(contaPagar: ContaPagar): any {
    return this.http.put(`${this.contasPagarURL}/${contaPagar.id}`, contaPagar);
  }

  delete(contaPagar: ContaPagar): any {
    return this.http.delete(`${this.contasPagarURL}/${contaPagar.id}`);
  }

  findByPage(direction, orderBy, linesPerPage, page): Observable<Paginator<ContaPagar>> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<ContaPagar>>(`${this.contasPagarURL}/page?direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }
}
