import { Despesa } from './../../../cadastro/despesa/shared/despesa.model';

export class ContaPagar {
  id?: number;
  dataPagamento?: string;
  tipoDespesa?: Despesa;
  numeroDocumento?: string;
  valor?: number;
  observacao?: string;
  statusTitulo?: string;
}
