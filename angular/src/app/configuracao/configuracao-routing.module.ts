import { AcessoComponent } from './acesso/acesso.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from './../login/auth.guard';

import { UsuarioComponent } from './usuario/usuario.component';

const ROUTES: Routes = [
    {
        path: 'usuario',
        component: UsuarioComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'acesso',
        component: AcessoComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class ConfiguracaoRoutingModule { }
