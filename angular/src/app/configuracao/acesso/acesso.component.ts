import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSnackBar, MatDialog } from '@angular/material';

import { tap } from 'rxjs/operators';

import { Acesso } from './shared/acesso.model';
import { Permissao } from './../permissao/shared/permissao.model';
import { Usuario } from '../usuario/shared/usuario.model';

import { AcessoService } from './shared/acesso.service';
import { PermissaoService } from './../permissao/shared/permissao.service';
import { UsuarioService } from '../usuario/shared/usuario.service';

import { ConfirmacaoComponent } from './../../shared/componentes-globais/confirmacao/confirmacao.component';
import { EDITAR_USUARIO, PERMISSAO_EXISTENTE, CONFIRMACAO_DELETE, DELETAR_RECEITA } from './../../shared/texto-consts/texto-consts';
import { PAGE_SIZES } from './../../shared/tabela/tabela';

@Component({
  selector: 'app-acesso',
  templateUrl: './acesso.component.html',
  styleUrls: ['./acesso.component.css']
})

export class AcessoComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'usuario',
    'permissao',
    'excluir'
  ];

  dataSource: MatTableDataSource<Acesso>;

  acesso: Acesso;
  usuario: Usuario;
  permissao: Permissao;

  usuarios: Usuario[];
  permissoes: Permissao[];

  editing: boolean;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  constructor(private usuarioService: UsuarioService,
              private permissaoService: PermissaoService,
              private acessoService: AcessoService,
              private dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Acesso>();
    this.checarAcesso();
    this.iniciarPaginacao();
    this.carregarTabela();
    this.filtrarUsuarios();
    this.filtrarPermissoes();
  }

  checarAcesso() {
    if (this.acesso === undefined) {
      this.resetar();
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  resetar() {
    this.acesso = {
      usuario: {id: null},
      permissao: {id: null},
    };
  }

  resetarEditando() {
    this.acesso = {
      usuario: null,
      permissao: null,
    };
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.carregarTabela();
      })
    ).subscribe();
  }

  popularTabela(dataSource: Acesso[]) {
    this.dataSource.data = dataSource;
  }

  carregarTabela() {
    this.acessoService.findByPage(
      this.direction = 'ASC',
      this.orderBy = 'nome',
      this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
      this.paginator.pageIndex ? this.paginator.pageIndex : 0
    ).subscribe(res => {
      this.popularTabela(res.content);
      this.paginatorLength = res.totalElements;
    });
  }

  filtrarUsuarios() {
    this.usuarioService.findByUsuarioExcetoAdm().subscribe(res => {
      this.usuarios = res;
    });
  }

  atribuirUsuario(ev) {
    if (ev.source.value !== null && ev.source.value !== undefined) {
      this.usuario = this.usuarios.find(element => {
        return element.id === ev.source.value;
      });
    } else {
      this.usuario = { id: null };
    }
  }

  filtrarPermissoes() {
    this.permissaoService.findAll().subscribe(res => {
      this.permissoes = res;
    });
  }

  atribuirPermissao(ev) {
    if (ev.source.value !== null && ev.source.value !== undefined) {
      this.usuario.permissoes = this.permissoes.filter(element => {
        return element.id === ev.source.value;
      });
    } else {
      this.permissao = { id: null };
    }
  }

  salvarPermissaoUsuario() {
    this.usuarioService.updateAcess(this.usuario).subscribe(() => {
      this.resetar();
      this.carregarTabela();
      this.snackBar.open(EDITAR_USUARIO, 'X', {
        duration: 3000
      });
    }, errorSalvar => {
      this.snackBar.open(PERMISSAO_EXISTENTE, 'X', {
        duration: 3000
      });
    });
  }

  excluir(row: Acesso) {
    this.usuarioService.deleteByPermission(row).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(DELETAR_RECEITA, 'X', {
        duration: 2000
      });
    }, errorExcluir => {
      this.snackBar.open(errorExcluir.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  deletarDialogo(acesso: Acesso) {
    const dialogRef = this.dialog.open(ConfirmacaoComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_DELETE;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(acesso);
      }
    });
  }

}
