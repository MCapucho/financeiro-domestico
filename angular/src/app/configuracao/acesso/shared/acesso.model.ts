import { Permissao } from './../../permissao/shared/permissao.model';
import { Usuario } from './../../usuario/shared/usuario.model';
export class Acesso {
    nome?: string;
    descricao?: string;
    usuario?: Usuario;
    permissao?: Permissao;
}
