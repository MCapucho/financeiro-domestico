import { Paginator } from './../../../shared/model/paginator.model';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { Acesso } from './acesso.model';

import { LoginHttp } from './../../../login/shared/login-http';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AcessoService {

  dataChange: BehaviorSubject<Acesso[]> = new BehaviorSubject<Acesso[]>([]);

  acessosURL: string;

  constructor(private http: LoginHttp) {
    this.acessosURL = `${environment.apiUrl}/acessos`;
  }

  findByPage(direction, orderBy, linesPerPage, page): Observable<Paginator<Acesso>> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<Acesso>>(`${this.acessosURL}/page?direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }
}
