import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { Permissao } from './permissao.model';

import { LoginHttp } from './../../../login/shared/login-http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PermissaoService {

  dataChange: BehaviorSubject<Permissao[]> = new BehaviorSubject<Permissao[]>([]);

  permissoesURL: string;

  constructor(private http: LoginHttp) {
    this.permissoesURL = `${environment.apiUrl}/permissoes`;
  }

  findAll(): Observable<Permissao[]> {
    return this.http.get<Permissao[]>(this.permissoesURL);
  }
}
