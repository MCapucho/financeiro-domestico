import { NgModule } from '@angular/core';

import { CoreModule } from '../core/core.module';
import { ConfiguracaoRoutingModule } from './configuracao-routing.module';

import { DialogSenhaComponent } from './usuario/dialog-senha/dialog-senha.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AcessoComponent } from './acesso/acesso.component';

@NgModule({
  declarations: [
    UsuarioComponent,
    AcessoComponent,
    DialogSenhaComponent,
  ],

  imports: [
    CoreModule,

    ConfiguracaoRoutingModule
  ],

  exports: [
    UsuarioComponent,
    AcessoComponent
  ],

  entryComponents: [
    DialogSenhaComponent
  ]
})

export class ConfiguracaoModule {}
