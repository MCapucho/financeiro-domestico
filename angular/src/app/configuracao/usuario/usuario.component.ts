import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';

import * as _ from 'lodash';
import { tap } from 'rxjs/operators';

import { Usuario } from './shared/usuario.model';

import { UsuarioService } from './shared/usuario.service';

import { DialogSenhaComponent } from './dialog-senha/dialog-senha.component';

import { ConfirmacaoComponent } from './../../shared/componentes-globais/confirmacao/confirmacao.component';
import { CRIAR_USUARIO, EDITAR_USUARIO, DELETAR_USUARIO, CONFIRMACAO_DELETE, CONFIRMACAO_ALTERAR_SENHA,
         EDITAR_SENHA_USUARIO } from './../../shared/texto-consts/texto-consts';
import { PAGE_SIZES } from './../../shared/tabela/tabela';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})

export class UsuarioComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'nome',
    'email',
    'senha',
    'editar',
    'excluir'
  ];

  dataSource: MatTableDataSource<Usuario>;

  usuario: Usuario;

  editing: boolean;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  constructor(private usuarioService: UsuarioService,
              private dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Usuario>();
    this.checarUsuario();
    this.iniciarPaginacao();
    this.carregarTabela();
  }

  checarUsuario() {
    if (this.usuario === undefined) {
      this.resetar();
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  resetar() {
    this.usuario = {
      nome: null,
      email: null,
      senha: null
    };
  }

  resetarEditando() {
    this.usuario = {
      id: this.usuario.id,
      nome: null,
      email: null,
      senha: null
    };
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.carregarTabela();
      })
    ).subscribe();
  }

  popularTabela(dataSource: Usuario[]) {
    this.dataSource.data = dataSource;
  }

  carregarTabela() {
    this.usuarioService.findByPage(
      this.direction = 'ASC',
      this.orderBy = 'nome',
      this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
      this.paginator.pageIndex ? this.paginator.pageIndex : 0
    ).subscribe(res => {
      this.popularTabela(res.content);
      this.paginatorLength = res.totalElements;
    });
  }

  criar() {
    this.usuarioService.create(this.usuario).subscribe(result => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(CRIAR_USUARIO, 'X', {
        duration: 3000
      });
    }, errorCriar => {
      this.snackBar.open(errorCriar.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  atualizar(row: Usuario) {
    const oldRegistry = _.cloneDeep(row);

    this.usuario = oldRegistry;
    this.usuario.senha = '';
    this.checarUsuario();
  }

  editar() {
    this.usuarioService.update(this.usuario).subscribe(() => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(EDITAR_USUARIO, 'X', {
        duration: 3000
      });
    }, errorEditar => {
      this.snackBar.open(errorEditar.error.userMessage, 'X', {
        duration: 3000
      });
    });

    this.usuario = undefined;
    this.checarUsuario();
    this.resetar();
  }

  excluir(usuario: Usuario) {
    this.usuarioService.delete(usuario).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(DELETAR_USUARIO, 'X', {
        duration: 2000
      });
    }, errorExcluir => {
      this.snackBar.open(errorExcluir.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  deletarDialogo(row: Usuario) {
    const dialogRef = this.dialog.open(ConfirmacaoComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_DELETE;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(row);
      }
    });
  }

  alterarSenha(usuario: Usuario) {
    this.usuarioService.updatePassword(usuario).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(EDITAR_SENHA_USUARIO, 'X', {
        duration: 2000
      });
    }, errorExcluir => {
      this.snackBar.open(errorExcluir.error.message, 'X', {
        duration: 3000
      });
    });
  }

  senhaDialogo(row: Usuario) {
    const dialogRef = this.dialog.open(DialogSenhaComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_ALTERAR_SENHA;

    const oldRegistry = _.cloneDeep(row);

    dialogRef.componentInstance.usuario = oldRegistry;

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.alterarSenha(dialogRef.componentInstance.usuario);
      }
    });
  }

  cancelar() {
    this.usuario = undefined;
    this.checarUsuario();
    this.resetar();
  }

}
