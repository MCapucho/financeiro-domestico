import { Component, OnInit } from '@angular/core';
import { Usuario } from '../shared/usuario.model';

@Component({
  selector: 'app-dialog-senha',
  templateUrl: './dialog-senha.component.html',
  styleUrls: ['./dialog-senha.component.css']
})
export class DialogSenhaComponent implements OnInit {

  mensagem: string;

  usuario: Usuario;

  constructor() { }

  ngOnInit() {
    this.usuario.senha = '';
  }

}
