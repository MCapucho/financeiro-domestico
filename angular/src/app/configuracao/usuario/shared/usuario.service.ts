import { Paginator } from './../../../shared/model/paginator.model';
import { Permissao } from './../../permissao/shared/permissao.model';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { Usuario } from './usuario.model';

import { LoginHttp } from 'src/app/login/shared/login-http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  dataChange: BehaviorSubject<Usuario[]> = new BehaviorSubject<Usuario[]>([]);

  usuariosURL: string;

  constructor(private http: LoginHttp) {
    this.usuariosURL = `${environment.apiUrl}/usuarios`;
  }

  create(usuario: Usuario): any {
    return this.http.post(this.usuariosURL, usuario);
  }

  update(usuario: Usuario): any {
    return this.http.put(`${this.usuariosURL}/${usuario.id}`, usuario);
  }

  updatePassword(usuario: Usuario): any {
    return this.http.put(`${this.usuariosURL}/senha/${usuario.id}`, usuario);
  }

  updateAcess(usuario: Usuario): any {
    return this.http.put(`${this.usuariosURL}/acesso/${usuario.id}`, usuario);
  }

  delete(usuario: Usuario): any {
    return this.http.delete(`${this.usuariosURL}/${usuario.id}`, usuario);
  }

  deleteByPermission(usuarioPermissao: Permissao): any {
    return this.http.delete(`${this.usuariosURL}/${usuarioPermissao.descricao}`);
  }

  findAll(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(this.usuariosURL);
  }

  findByUsuarioExcetoAdm(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(`${this.usuariosURL}/excetoAdm`);
  }

  findByPage(direction, orderBy, linesPerPage, page): Observable<Paginator<Usuario>> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<Usuario>>(`${this.usuariosURL}/page?direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }
}
