import { Permissao } from '../../permissao/shared/permissao.model';

export class Usuario {
    id?: number;
    nome?: string;
    email?: string;
    senha?: string;
    permissoes?: Permissao[];

    constructor() {
      this.permissoes = [];
    }
}
