import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ErrorHandlerService } from './../core/error-handler.service';
import { LoginService } from './shared/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private loginService: LoginService,
              private errorHandler: ErrorHandlerService,
              private router: Router) { }

  ngOnInit() {

  }

  getLogin(usuario: string, senha: string) {
    this.loginService.login(usuario, senha)
      .then(() => {
        this.router.navigate(['/home']);
      })
      .catch(erro => {
        this.errorHandler.handle(erro);
      });
  }

}
