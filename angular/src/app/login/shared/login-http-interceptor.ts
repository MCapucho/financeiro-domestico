import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { LoginService } from './login.service';

export class NotAuthenticatedError {}

@Injectable({
    providedIn: 'root'
})

export class LoginHttpInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('token');

        if (token && request.url.indexOf('/oauth/token') === -1) {
          request = request.clone({
            setHeaders: {
              Accept: `application/json`,
              'Content-Type': `application/json`,
              Authorization: `Bearer ${token}`
            }
          });
        }

        return next.handle(request);
    }
}
