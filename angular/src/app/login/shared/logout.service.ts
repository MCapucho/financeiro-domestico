import { Injectable } from '@angular/core';

import { LoginService } from './login.service';

import { LoginHttp } from './login-http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class LogoutService {

  tokenRevokeURL: string;

  constructor(private http: LoginHttp,
              private loginService: LoginService) {
    this.tokenRevokeURL = `${environment.apiUrl}/tokens/revoke`;
  }

  logout() {
    return this.http.delete(this.tokenRevokeURL, { withCredentials: true})
      .toPromise()
      .then(() => {
        this.loginService.limparAccessToken();
      });
  }

}
