import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { CoreModule } from './../core/core.module';

import { JwtModule } from '@auth0/angular-jwt';

import { LoginComponent } from './login.component';

import { environment } from './../../environments/environment';
import { AuthGuard } from './auth.guard';

export function tokenGetter(): string {
  return localStorage.getItem('token');
}
@NgModule
({
  declarations: [
    LoginComponent
  ],

  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,

    JwtModule.forRoot({
      config: {
        // tslint:disable-next-line: object-literal-shorthand
        tokenGetter: tokenGetter,
      }
    })
  ],

  exports: [
    LoginComponent
  ],

  providers: [
    AuthGuard,
  ],

  entryComponents: []
})

export class LoginModule {}
