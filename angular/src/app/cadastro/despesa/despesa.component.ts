import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSnackBar, MatDialog } from '@angular/material';

import * as _ from 'lodash';
import { tap } from 'rxjs/operators';

import { Despesa } from './shared/despesa.model';

import { DespesaService } from './shared/despesa.service';

import { ConfirmacaoComponent } from './../../shared/componentes-globais/confirmacao/confirmacao.component';
import { CRIAR_DESPESA, EDITAR_DESPESA, DELETAR_DESPESA, CONFIRMACAO_DELETE } from './../../shared/texto-consts/texto-consts';
import { PAGE_SIZES } from './../../shared/tabela/tabela';

@Component({
  selector: 'app-despesa',
  templateUrl: './despesa.component.html',
  styleUrls: ['./despesa.component.css']
})

export class DespesaComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'id',
    'descricao',
    'status',
    'editar',
    'excluir'
  ];

  dataSource: MatTableDataSource<Despesa>;

  despesa: Despesa;

  editing: boolean;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  constructor(private despesaService: DespesaService,
              private dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Despesa>();
    this.checarDespesa();
    this.iniciarPaginacao();
    this.carregarTabela();
  }

  checarDespesa() {
    if (this.despesa === undefined) {
      this.resetar();
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  resetar() {
    this.despesa = {
      descricao: null,
      status: true,
    };
  }

  resetarEditando() {
    this.despesa = {
      id: this.despesa.id,
      descricao: null,
      status: true
    };
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.carregarTabela();
      })
    ).subscribe();
  }

  popularTabela(dataSource: Despesa[]) {
    this.dataSource.data = dataSource;
  }

  carregarTabela() {
    this.despesaService.findByPage(
      this.direction = 'ASC',
      this.orderBy = 'id',
      this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
      this.paginator.pageIndex ? this.paginator.pageIndex : 0
    ).subscribe(res => {
      this.popularTabela(res.content);
      this.paginatorLength = res.totalElements;
    });
  }

  criar() {
    this.despesaService.create(this.despesa).subscribe(result => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(CRIAR_DESPESA, 'X', {
        duration: 3000
      });
    }, errorCriar => {
      this.snackBar.open(errorCriar.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  atualizar(row: Despesa) {
    const oldRegistry = _.cloneDeep(row);

    this.despesa = oldRegistry;

    this.checarDespesa();
  }

  editar() {
    this.despesaService.update(this.despesa).subscribe(() => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(EDITAR_DESPESA, 'X', {
        duration: 3000
      });
    }, errorEditar => {
      this.snackBar.open(errorEditar.error.userMessage, 'X', {
        duration: 3000
      });
    });

    this.despesa = undefined;
    this.checarDespesa();
    this.resetar();
  }

  excluir(row: Despesa) {
    this.despesaService.delete(row).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(DELETAR_DESPESA, 'X', {
        duration: 2000
      });
    }, errorExcluir => {
      this.snackBar.open(errorExcluir.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  deletarDialogo(despesa: Despesa) {
    const dialogRef = this.dialog.open(ConfirmacaoComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_DELETE;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(despesa);
      }
    });
  }

  cancelar() {
    this.despesa = undefined;
    this.checarDespesa();
    this.resetar();
  }
}

