import { Paginator } from './../../../shared/model/paginator.model';
import { Injectable } from '@angular/core';

import { Despesa } from './despesa.model';

import { BehaviorSubject, Observable } from 'rxjs';
import { LoginHttp } from 'src/app/login/shared/login-http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DespesaService {

  dataChange: BehaviorSubject<Despesa[]> = new BehaviorSubject<Despesa[]>([]);

  despesasURL: string;

  constructor(private http: LoginHttp) {
    this.despesasURL = `${environment.apiUrl}/despesas`;
  }

  create(despesa: Despesa): any {
    return this.http.post(this.despesasURL, despesa);
  }

  update(despesa: Despesa): any {
    return this.http.put(`${this.despesasURL}/${despesa.id}`, despesa);
  }

  delete(despesa: Despesa): any {
    return this.http.delete(`${this.despesasURL}/${despesa.id}`);
  }

  findAll(): Observable<Despesa[]> {
    return this.http.get<Despesa[]>(this.despesasURL);
  }

  findByStatusTrue(): Observable<Despesa[]> {
    return this.http.get<Despesa[]>(`${this.despesasURL}/ativo`);
  }

  findByPage(direction, orderBy, linesPerPage, page): Observable<Paginator<Despesa>> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<Despesa>>(`${this.despesasURL}/page?direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }
}
