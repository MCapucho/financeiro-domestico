import { NgModule } from '@angular/core';

import { CoreModule } from './../core/core.module';
import { CadastroRoutingModule } from './cadastro-routing.module';

import { ReceitaComponent } from './receita/receita.component';
import { DespesaComponent } from './despesa/despesa.component';

@NgModule({
  declarations: [
    ReceitaComponent,
    DespesaComponent
  ],

  imports: [
    CoreModule,

    CadastroRoutingModule
  ],

  exports: [
    ReceitaComponent,
    DespesaComponent
  ],

  entryComponents: []
})

export class CadastroModule {}
