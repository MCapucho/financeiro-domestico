import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSnackBar, MatDialog } from '@angular/material';

import { tap } from 'rxjs/operators';
import * as _ from 'lodash';

import { Receita } from './shared/receita.model';

import { ReceitaService } from './shared/receita.service';

import { PAGE_SIZES } from './../../shared/tabela/tabela';
import { ConfirmacaoComponent } from 'src/app/shared/componentes-globais/confirmacao/confirmacao.component';
import { CRIAR_RECEITA, DELETAR_RECEITA, EDITAR_RECEITA, CONFIRMACAO_DELETE } from './../../shared/texto-consts/texto-consts';


@Component({
  selector: 'app-receita',
  templateUrl: './receita.component.html',
  styleUrls: ['./receita.component.css']
})

export class ReceitaComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'id',
    'descricao',
    'status',
    'editar',
    'excluir'
  ];

  dataSource: MatTableDataSource<Receita>;

  receita: Receita;

  editing: boolean;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  constructor(private receitaService: ReceitaService,
              private dialog: MatDialog,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<Receita>();
    this.checarReceita();
    this.iniciarPaginacao();
    this.carregarTabela();
  }

  checarReceita() {
    if (this.receita === undefined) {
      this.resetar();
      this.editing = false;
    } else {
      this.editing = true;
    }
  }

  resetar() {
    this.receita = {
      descricao: null,
      status: true,
    };
  }

  resetarEditando() {
    this.receita = {
      id: this.receita.id,
      descricao: null,
      status: true
    };
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.carregarTabela();
      })
    ).subscribe();
  }

  popularTabela(dataSource: Receita[]) {
    this.dataSource.data = dataSource;
  }

  carregarTabela() {
    this.receitaService.findByPage(
      this.direction = 'ASC',
      this.orderBy = 'id',
      this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
      this.paginator.pageIndex ? this.paginator.pageIndex : 0
    ).subscribe(res => {
      this.popularTabela(res.content);
      this.paginatorLength = res.totalElements;
    });
  }

  criar() {
    this.receitaService.create(this.receita).subscribe(result => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(CRIAR_RECEITA, 'X', {
        duration: 3000
      });
    }, errorCriar => {
      this.snackBar.open(errorCriar.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  atualizar(row: Receita) {
    const oldRegistry = _.cloneDeep(row);

    this.receita = oldRegistry;

    this.checarReceita();
  }

  editar() {
    this.receitaService.update(this.receita).subscribe(() => {
      this.carregarTabela();
      this.resetar();
      this.snackBar.open(EDITAR_RECEITA, 'X', {
        duration: 3000
      });
    }, errorEditar => {
      this.snackBar.open(errorEditar.error.userMessage, 'X', {
        duration: 3000
      });
    });

    this.receita = undefined;
    this.checarReceita();
    this.resetar();
  }

  excluir(row: Receita) {
    this.receitaService.delete(row).subscribe(() => {
      this.carregarTabela();
      this.snackBar.open(DELETAR_RECEITA, 'X', {
        duration: 2000
      });
    }, errorExcluir => {
      this.snackBar.open(errorExcluir.error.userMessage, 'X', {
        duration: 3000
      });
    });
  }

  deletarDialogo(receita: Receita) {
    const dialogRef = this.dialog.open(ConfirmacaoComponent, {
      disableClose: true,
      width: '30%'
    });
    dialogRef.componentInstance.mensagem = CONFIRMACAO_DELETE;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.excluir(receita);
      }
    });
  }

  cancelar() {
    this.receita = undefined;
    this.checarReceita();
    this.resetar();
  }
}
