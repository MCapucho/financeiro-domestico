import { Paginator } from './../../../shared/model/paginator.model';
import { Injectable } from '@angular/core';

import { Receita } from './receita.model';

import { BehaviorSubject, Observable } from 'rxjs';
import { LoginHttp } from 'src/app/login/shared/login-http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ReceitaService {

  dataChange: BehaviorSubject<Receita[]> = new BehaviorSubject<Receita[]>([]);

  receitasURL: string;

  constructor(private http: LoginHttp) {
    this.receitasURL = `${environment.apiUrl}/receitas`;
  }

  create(receita: Receita): any {
    return this.http.post(this.receitasURL, receita);
  }

  update(receita: Receita): any {
    return this.http.put(`${this.receitasURL}/${receita.id}`, receita);
  }

  delete(receita: Receita): any {
    return this.http.delete(`${this.receitasURL}/${receita.id}`);
  }

  findAll(): Observable<Receita[]> {
    return this.http.get<Receita[]>(this.receitasURL);
  }

  findByStatusTrue(): Observable<Receita[]> {
    return this.http.get<Receita[]>(`${this.receitasURL}/ativo`);
  }

  findByPage(direction, orderBy, linesPerPage, page): Observable<Paginator<Receita>> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<Receita>>(`${this.receitasURL}/page?direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }
}
