import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from './../login/auth.guard';

import { ReceitaComponent } from './receita/receita.component';
import { DespesaComponent } from './despesa/despesa.component';

const ROUTES: Routes = [
    {
        path: 'receita',
        component: ReceitaComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_RECEITA_FILTRAR']}
    },
    {
        path: 'despesa',
        component: DespesaComponent,
        canActivate: [AuthGuard],
        data: { roles: ['ROLE_DESPESA_FILTRAR']}
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class CadastroRoutingModule { }
