import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { CoreModule } from './../core/core.module';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent
  ],

  imports: [
    CoreModule,
    RouterModule,
  ],

  exports: [
    HeaderComponent,
    FooterComponent
  ]
})

export class TemplateModule {}
