import { Component, OnInit } from '@angular/core';

import { LoginService } from 'src/app/login/shared/login.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  usuarioLogado: any;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.getUsuarioLogado();
  }

  getUsuarioLogado() {
    this.usuarioLogado = this.loginService.jwtPayload.nome;
  }

}
