import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';

import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt, 'pt-BR');

import { HomeService } from './shared/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt-BR'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class HomeComponent implements OnInit {

  totalContaReceber: any;
  totalContaPagar: any;
  totalFinal: any;
  todoPeriodoContaPagar: any;

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.totalizadorContaReceber();
    this.totalizadorContaPagar();
    this.totalizadorFinal();
    this.totalizadorTotalContaPagar();
  }

  totalizadorContaReceber() {
    this.homeService.totalizadorContaReceber().subscribe(res => {
      this.totalContaReceber = res;
    });
  }

  totalizadorContaPagar() {
    this.homeService.totalizadorContaPagar().subscribe(res => {
      this.totalContaPagar = res;
    });
  }

  totalizadorFinal() {
    this.homeService.totalizadorFinal().subscribe(res => {
      this.totalFinal = res;
    });
  }

  totalizadorTotalContaPagar() {
    this.homeService.totalizadorTotalContaPagar().subscribe(res => {
      this.todoPeriodoContaPagar = res;
    });
  }
}
