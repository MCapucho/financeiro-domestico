import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { API_URL } from '../../../app.api';

import { BehaviorSubject, Observable } from 'rxjs';

import { ContaPagar } from '../../../lancamento/conta-pagar/shared/conta-pagar.model';
import { ContaReceber } from '../../../lancamento/conta-receber/shared/conta-receber.model';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  dataChange: BehaviorSubject<ContaReceber[]> = new BehaviorSubject<ContaReceber[]>([]);

  homeURL: string;

  constructor(private http: HttpClient) {
    this.homeURL = `${environment.apiUrl}/home`;
  }

  totalizadorContaReceber(): Observable<ContaReceber[]> {
    return this.http.get<ContaReceber[]>(`${this.homeURL}/totalContaReceber`);
  }

  totalizadorContaPagar(): Observable<ContaPagar[]> {
    return this.http.get<ContaPagar[]>(`${this.homeURL}/totalContaPagar`);
  }

  totalizadorFinal(): Observable<any[]> {
    return this.http.get<any[]>(`${this.homeURL}/totalFinal`);
  }

  totalizadorTotalContaPagar(): Observable<ContaPagar[]> {
    return this.http.get<ContaPagar[]>(`${this.homeURL}/todoPeriodoContaPagar`);
  }
}
