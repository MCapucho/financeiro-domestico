import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';

import { CoreModule } from './../core/core.module';
import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  declarations: [
    HomeComponent
  ],

  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,

    HomeRoutingModule
  ],

  exports: [
    HomeComponent
  ]
})

export class HomeModule {}
