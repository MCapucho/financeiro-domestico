import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { LoginHttpInterceptor } from './login/shared/login-http-interceptor';

import { AppComponent } from './app.component';
import { ConfirmacaoComponent } from './shared/componentes-globais/confirmacao/confirmacao.component';
import { LoaderComponent } from './shared/componentes-globais/loader/loader.component';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { LoaderInterceptor } from './shared/interceptor/loader/loader.interceptor';
import { LoaderService } from './shared/interceptor/loader/loader.service';
import { LoginModule } from './login/login.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxMaskModule } from 'ngx-mask';
import { TemplateModule } from './template/template.module';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmacaoComponent,
    LoaderComponent,
  ],

  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    FlexLayoutModule,
    FontAwesomeModule,
    HttpClientModule,
    LoginModule,
    NgxMaskModule.forRoot(),
    TemplateModule,
  ],

  entryComponents: [
    ConfirmacaoComponent
  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: LoginHttpInterceptor, multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true
    },
    LoaderService,
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }
