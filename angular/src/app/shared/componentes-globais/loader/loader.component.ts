import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';

import { LoaderService } from './../../interceptor/loader/loader.service';
import { Subscription } from 'rxjs';
import { LoaderState } from './loader.state';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit, OnDestroy {

  loading = [];
  private subscription: Subscription;

  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    this._startSubscription();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('document: keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (this.loading.length > 0) {
      event.preventDefault();
    }
  }

  private _startSubscription() {
    this.subscription = this.loaderService.loaderState
    .subscribe((state: LoaderState) => {
      this.loading = state.show;
    });
  }

}
