import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})

export class ButtonComponent implements OnInit {

  @Input() editing = false;
  @Input() tipoBotao = '';
  @Input() validacao = '';
  @Input() textoBotao = '';

  @Output() eventoBotaoOutput = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  eventoBotao() {
    this.eventoBotaoOutput.emit();
  }

}
