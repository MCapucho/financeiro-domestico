export const CRIAR_RECEITA = 'A receita foi criada com sucesso';
export const EDITAR_RECEITA = 'A receita foi atualizada com sucesso';
export const DELETAR_RECEITA = 'A receita foi excluída com sucesso';

export const CRIAR_DESPESA = 'A despesa foi criada com sucesso';
export const EDITAR_DESPESA = 'A despesa foi atualizada com sucesso';
export const DELETAR_DESPESA = 'A despesa foi excluída com sucesso';

export const LANÇAR_CONTA_RECEBER = 'O lançamento à receber foi criado com sucesso';
export const EDITAR_CONTA_RECEBER = 'O lançamento à receber foi atualizado com sucesso';
export const DELETAR_CONTA_RECEBER = 'O lançamento à receber foi excluído com sucesso';

export const LANÇAR_CONTA_PAGAR = 'O lançamento à pagar foi criado com sucesso';
export const EDITAR_CONTA_PAGAR = 'O lançamento à pagar foi atualizado com sucesso';
export const DELETAR_CONTA_PAGAR = 'O lançamento à pagar foi excluído com sucesso';

export const CRIAR_USUARIO = 'O usuário foi criado com sucesso';
export const EDITAR_USUARIO = 'O usuário foi atualizado com sucesso';
export const DELETAR_USUARIO = 'O usuário foi excluído com sucesso';
export const EDITAR_SENHA_USUARIO = 'A senha do usuário foi atualizado com sucesso';

export const DATA_CHECK = 'A data final está menor que a data inicial';

export const CONFIRMACAO_DELETE = 'Deseja realmente excluir?';

export const CONFIRMACAO_ALTERAR_SENHA = 'Alteração de Senha';

export const QUITAR_TITULO = 'Título quitado com sucesso';

export const PERMISSAO_EXISTENTE = 'Permissão já existente no usuário informado';
