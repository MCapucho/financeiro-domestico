import { Injectable } from '@angular/core';
import {
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { LoaderService } from './loader.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const deactivateLoader = req.params.get('deactivateLoader');
    if (!deactivateLoader) {
      this._showLoader(req.url + req.method);
      return next.handle(req).pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            this._onEnd(req.url + req.method);
          }
        }, error => {
          this._onEnd(req.url + req.method);
        })
      );
    } else { return next.handle(req); }
  }

  private _onEnd(req: string): void {
    this._hideLoader(req);
  }

  private _showLoader(req: string): void {
    this.loaderService.show(req);
  }

  private _hideLoader(req: string): void {
    this.loaderService.hide(req);
  }
}
