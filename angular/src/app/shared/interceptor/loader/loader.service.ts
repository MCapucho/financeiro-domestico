import { LoaderState } from './../../componentes-globais/loader/loader.state';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LoaderService {

  private loading: LoaderState;
  private loaderSubject = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();

  constructor() {
    this.loading = {show: []};
  }

  show(req: string) {
    this.loading.show.push(req);
    this.loaderSubject.next({show: [...this.loading.show]});
  }

  hide(req: string) {
    this.loading.show.splice(this.loading.show.findIndex(val => val === req), 1);
    this.loaderSubject.next({ show: [...this.loading.show]});
  }

}
