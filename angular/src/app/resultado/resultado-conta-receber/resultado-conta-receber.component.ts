import { Component, OnInit, LOCALE_ID, ViewChild } from '@angular/core';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MatSnackBar, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatTableDataSource, MatPaginator } from '@angular/material';

import { tap } from 'rxjs/operators';

import { ContaReceber } from 'src/app/lancamento/conta-receber/shared/conta-receber.model';

import { ResultadoService } from './../shared/resultado.service';

import { DATA_CHECK } from './../../shared/texto-consts/texto-consts';
import { PAGE_SIZES } from './../../shared/tabela/tabela';

@Component({
  selector: 'app-resultado-conta-receber',
  templateUrl: './resultado-conta-receber.component.html',
  styleUrls: ['./resultado-conta-receber.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ResultadoContaReceberComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = [
    'dataRecebimento',
    'tipoReceita',
    'numeroDocumento',
    'observacao',
    'statusTitulo',
    'valor'
  ];

  dataSource: MatTableDataSource<ContaReceber>;

  dataInicial;
  dataFinal;

  direction: string;
  orderBy: string;
  paginatorLength: number;

  desabilitadoBuscar = true;

  totalContaReceber: number;

  constructor(private resultadoService: ResultadoService,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<ContaReceber>();
    this.iniciarPaginacao();
    this.totalReceita();
  }

  checarDataFinal() {
    if (this.dataInicial && this.dataFinal) {
      if (this.dataInicial._d > this.dataFinal._d) {
        this.dataFinal = {};
        this.desabilitadoBuscar = true;
        this.snackBar.open(DATA_CHECK, 'X', {
          duration: 5000
        });
      } else {
        this.desabilitadoBuscar = false;
      }
    }
  }

  iniciarPaginacao() {
    this.paginator.pageSizeOptions = PAGE_SIZES;
    this.paginator.page.pipe(
      tap(() => {
        this.filtrarDatas();
      })
    ).subscribe();
  }

  filtrarDatas() {
    if (this.dataInicial && this.dataFinal) {
      this.resultadoService.resultByPeriodContaReceber(
        this.dataInicial,
        this.dataFinal,
        this.direction = 'ASC',
        this.orderBy = 'dataRecebimento',
        this.paginator.pageSize ? this.paginator.pageSize : this.paginator.pageSizeOptions[0],
        this.paginator.pageIndex ? this.paginator.pageIndex : 0
      ).subscribe(res => {
        this.popularTabela(res.content);
        this.paginatorLength = res.totalElements;
        this.totalReceita();
      });
    }
  }

  popularTabela(dataSource: ContaReceber[]) {
    this.dataSource.data = dataSource;
  }

  resetar() {
    this.dataInicial = null;
    this.dataFinal = null;
    this.popularTabela(this.dataSource.data = null);
    this.totalContaReceber = null;
  }

  totalReceita() {
    this.dataSource.data.forEach(resp => {
      // tslint:disable-next-line: only-arrow-functions
      this.totalContaReceber = this.dataSource.data.reduce(function(accumulator, elem) {
          return accumulator + Number(elem.valor);
        }, 0);
      }
    );
  }
}
