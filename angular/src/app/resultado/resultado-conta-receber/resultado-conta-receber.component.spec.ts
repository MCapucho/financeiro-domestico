import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoContaReceberComponent } from './resultado-conta-receber.component';

describe('ResultadoContaReceberComponent', () => {
  let component: ResultadoContaReceberComponent;
  let fixture: ComponentFixture<ResultadoContaReceberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoContaReceberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoContaReceberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
