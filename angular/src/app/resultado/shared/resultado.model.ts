import { ContaReceber } from './../../lancamento/conta-receber/shared/conta-receber.model';
import { ContaPagar } from './../../lancamento/conta-pagar/shared/conta-pagar.model';

export class Resultado {
    contasDespesasQuitadas?: ContaPagar[];
    contasDespesasAbertas?: ContaPagar[];
    contasReceitasAbertas?: ContaReceber[];
    contasReceitasQuitadas?: ContaReceber[];
}
