import { Paginator } from './../../shared/model/paginator.model';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { ContaReceber } from 'src/app/lancamento/conta-receber/shared/conta-receber.model';
import { ContaPagar } from 'src/app/lancamento/conta-pagar/shared/conta-pagar.model';

import { environment } from 'src/environments/environment';
import { LoginHttp } from 'src/app/login/shared/login-http';

@Injectable({
  providedIn: 'root'
})
export class ResultadoService {

  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

  resultadosURL: string;

  constructor(private http: LoginHttp) {
    this.resultadosURL = `${environment.apiUrl}/resultados`;
  }

  formatarData(value): string {
    const option = {year: 'numeric', month: '2-digit', day: '2-digit'};
    const data = new Date(value);
    const dataLocal = data.toLocaleDateString('en-US', option);
    const vetData = dataLocal.split('/');
    return `${vetData[2]}-${vetData[0]}-${vetData[1]}`;
  }

  resultByPeriodContaReceber(dataInicial, dataFinal, direction, orderBy, linesPerPage, page): Observable<Paginator<ContaReceber>> {
    const stringDataInicial = this.formatarData(dataInicial);
    const stringDataFinal = this.formatarData(dataFinal);

    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<ContaReceber>>(`${this.resultadosURL}/contaReceber?dataFinal=${stringDataFinal}&dataInicial=${stringDataInicial}&direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }

  resultByPeriodContaPagar(dataInicial, dataFinal, direction, orderBy, linesPerPage, page): Observable<Paginator<ContaPagar>> {
    const stringDataInicial = this.formatarData(dataInicial);
    const stringDataFinal = this.formatarData(dataFinal);

    // tslint:disable-next-line: max-line-length
    return this.http.get<Paginator<ContaPagar>>(`${this.resultadosURL}/contaPagar?dataFinal=${stringDataFinal}&dataInicial=${stringDataInicial}&direction=${direction}&linesPerPage=${linesPerPage}&orderBy=${orderBy}&page=${page}`);
  }

  resultByPeriodFinal(dataInicial, dataFinal): Observable<any[]> {
    const stringDataInicial = this.formatarData(dataInicial);
    const stringDataFinal = this.formatarData(dataFinal);

    return this.http.get<any[]>(`${this.resultadosURL}/final?` +
                    'dataFinal=' + stringDataFinal + '&dataInicial=' + stringDataInicial);
  }
}
