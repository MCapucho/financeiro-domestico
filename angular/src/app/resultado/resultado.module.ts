import { NgModule } from '@angular/core';

import { CoreModule } from './../core/core.module';
import { ResultadoRoutingModule } from './resultado-routing.module';

import { ResultadoContaPagarComponent } from './resultado-conta-pagar/resultado-conta-pagar.component';
import { ResultadoContaReceberComponent } from './resultado-conta-receber/resultado-conta-receber.component';
import { ResultadoFinalComponent } from './resultado-final/resultado-final.component';

@NgModule({
  declarations: [
    ResultadoContaPagarComponent,
    ResultadoContaReceberComponent,
    ResultadoFinalComponent
  ],

  imports: [
    CoreModule,

    ResultadoRoutingModule
  ],

  exports: [
    ResultadoContaPagarComponent,
    ResultadoContaReceberComponent,
    ResultadoFinalComponent
  ]
})

export class ResultadoModule {}
