import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoContaPagarComponent } from './resultado-conta-pagar.component';

describe('ResultadoContaPagarComponent', () => {
  let component: ResultadoContaPagarComponent;
  let fixture: ComponentFixture<ResultadoContaPagarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoContaPagarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoContaPagarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
