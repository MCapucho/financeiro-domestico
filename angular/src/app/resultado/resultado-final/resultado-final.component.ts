import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { MatSnackBar, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatTableDataSource } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';

import { ContaReceber } from './../../lancamento/conta-receber/shared/conta-receber.model';
import { ContaPagar } from './../../lancamento/conta-pagar/shared/conta-pagar.model';

import { ResultadoService } from './../shared/resultado.service';

import { DATA_CHECK } from './../../shared/texto-consts/texto-consts';

@Component({
  selector: 'app-resultado-final',
  templateUrl: './resultado-final.component.html',
  styleUrls: ['./resultado-final.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' },
    { provide: LOCALE_ID, useValue: 'pt'},
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})

export class ResultadoFinalComponent implements OnInit {

  displayedColumnsReceita: string[] = [
    'dataRecebimento',
    'tipoReceita',
    'numeroDocumento',
    'observacao',
    'statusTitulo',
    'valor'
  ];

  displayedColumnsDespesa: string[] = [
    'dataPagamento',
    'tipoDespesa',
    'numeroDocumento',
    'observacao',
    'statusTitulo',
    'valor'
  ];

  dataSourceReceitaAberta: MatTableDataSource<ContaReceber>;
  dataSourceReceitaQuitada: MatTableDataSource<ContaReceber>;

  dataSourceDespesaAberta: MatTableDataSource<ContaPagar>;
  dataSourceDespesaQuitada: MatTableDataSource<ContaPagar>;

  dataInicial;
  dataFinal;

  desabilitadoBuscar = true;

  totalContaReceberAberta: number;
  totalContaReceberQuitada: number;

  totalContaPagarAberta: number;
  totalContaPagarQuitada: number;

  totalContaReceberTodas: number;
  totalContaPagarTodas: number;

  totalFinal: number;
  totalFinalAbertas: number;
  totalFinalQuitadas: number;

  constructor(private resultadoService: ResultadoService,
              public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.dataSourceReceitaAberta = new MatTableDataSource<ContaReceber>();
    this.dataSourceReceitaQuitada = new MatTableDataSource<ContaReceber>();
    this.dataSourceDespesaAberta = new MatTableDataSource<ContaPagar>();
    this.dataSourceDespesaQuitada = new MatTableDataSource<ContaPagar>();

    this.totalReceitaAberta();
    this.totalReceitaQuitada();
    this.totalDespesaAberta();
    this.totalDespesaQuitada();
  }

  checarDataFinal() {
    if (this.dataInicial && this.dataFinal) {
      if (this.dataInicial._d > this.dataFinal._d) {
        this.dataFinal = {};
        this.desabilitadoBuscar = true;
        this.snackBar.open(DATA_CHECK, 'X', {
          duration: 5000
        });
      } else {
        this.desabilitadoBuscar = false;
      }
    }
  }

  filtrarDatas() {
    if (this.dataInicial && this.dataFinal) {
      this.resultadoService.resultByPeriodFinal(this.dataInicial, this.dataFinal).subscribe((res: any) => {
        this.popularTabelaReceitaAberta(res.contasReceitasAbertas);
        this.totalReceitaAberta();

        this.popularTabelaReceitaQuitada(res.contasReceitasQuitadas);
        this.totalReceitaQuitada();

        this.popularTabelaDespesaAberta(res.contasDespesasAbertas);
        this.totalDespesaAberta();

        this.popularTabelaDespesaQuitada(res.contasDespesasQuitadas);
        this.totalDespesaQuitada();

        this.totalContaReceber();
        this.totalContaPagar();

        this.totalContasFinal();
        this.totalContasFinalAbertas();
        this.totalContasFinalQuitadas();
      });
    }
  }

  popularTabelaReceitaAberta(dataSource: ContaReceber[]) {
    this.dataSourceReceitaAberta.data = dataSource;
  }

  popularTabelaReceitaQuitada(dataSource: ContaReceber[]) {
    this.dataSourceReceitaQuitada.data = dataSource;
  }

  popularTabelaDespesaAberta(dataSource: ContaPagar[]) {
    this.dataSourceDespesaAberta.data = dataSource;
  }

  popularTabelaDespesaQuitada(dataSource: ContaReceber[]) {
    this.dataSourceDespesaQuitada.data = dataSource;
  }

  resetar() {
    this.dataInicial = null;
    this.dataFinal = null;

    this.popularTabelaReceitaAberta(this.dataSourceReceitaAberta.data = null);
    this.popularTabelaReceitaQuitada(this.dataSourceReceitaQuitada.data = null);
    this.popularTabelaReceitaAberta(this.dataSourceReceitaAberta.data = null);
    this.popularTabelaReceitaQuitada(this.dataSourceReceitaQuitada.data = null);

    this.totalContaReceberAberta = null;
    this.totalContaReceberQuitada = null;
    this.totalContaPagarAberta = null;
    this.totalContaPagarQuitada = null;
  }

  totalReceitaAberta() {
    this.dataSourceReceitaAberta.data.forEach(resp => {
      // tslint:disable-next-line: only-arrow-functions
      this.totalContaReceberAberta = this.dataSourceReceitaAberta.data.reduce(function(accumulator, elem) {
          return accumulator + Number(elem.valor);
        }, 0);
      }
    );
  }

  totalReceitaQuitada() {
    this.dataSourceReceitaQuitada.data.forEach(resp => {
      // tslint:disable-next-line: only-arrow-functions
      this.totalContaReceberQuitada = this.dataSourceReceitaQuitada.data.reduce(function(accumulator, elem) {
          return accumulator + Number(elem.valor);
        }, 0);
      }
    );
  }

  totalDespesaAberta() {
    this.dataSourceDespesaAberta.data.forEach(resp => {
      // tslint:disable-next-line: only-arrow-functions
      this.totalContaPagarAberta = this.dataSourceDespesaAberta.data.reduce(function(accumulator, elem) {
          return accumulator + Number(elem.valor);
        }, 0);
      }
    );
  }

  totalDespesaQuitada() {
    this.dataSourceDespesaQuitada.data.forEach(resp => {
      // tslint:disable-next-line: only-arrow-functions
      this.totalContaPagarQuitada = this.dataSourceDespesaQuitada.data.reduce(function(accumulator, elem) {
          return accumulator + Number(elem.valor);
        }, 0);
      }
    );
  }

  totalContaReceber() {
    if (this.totalContaReceberQuitada === undefined) {
      this.totalContaReceberQuitada = 0;
    }

    if (this.totalContaReceberAberta === undefined) {
      this.totalContaReceberAberta = 0;
    }

    this.totalContaReceberTodas = this.totalContaReceberQuitada + this.totalContaReceberAberta;
  }

  totalContaPagar() {
    if (this.totalContaPagarQuitada === undefined) {
      this.totalContaPagarQuitada = 0;
    }

    if (this.totalContaPagarAberta === undefined) {
      this.totalContaPagarAberta = 0;
    }

    this.totalContaPagarTodas = this.totalContaPagarQuitada + this.totalContaPagarAberta;
  }

  totalContasFinal() {
    if (this.totalContaReceberTodas === undefined) {
      this.totalContaReceberTodas = 0;
    }

    if (this.totalContaPagarTodas === undefined) {
      this.totalContaPagarTodas = 0;
    }

    this.totalFinal = this.totalContaReceberTodas - this.totalContaPagarTodas;
  }

  totalContasFinalAbertas() {
    if (this.totalContaReceberAberta === undefined) {
      this.totalContaReceberAberta = 0;
    }

    if (this.totalContaPagarAberta === undefined) {
      this.totalContaPagarAberta = 0;
    }

    this.totalFinalAbertas = this.totalContaReceberAberta - this.totalContaPagarAberta;
  }

  totalContasFinalQuitadas() {
    if (this.totalContaReceberQuitada === undefined) {
      this.totalContaReceberQuitada = 0;
    }

    if (this.totalContaPagarQuitada === undefined) {
      this.totalContaPagarQuitada = 0;
    }

    this.totalFinalQuitadas = this.totalContaReceberQuitada - this.totalContaPagarQuitada;
  }
}
