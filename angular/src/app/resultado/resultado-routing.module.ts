import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from './../login/auth.guard';

import { ResultadoFinalComponent } from './resultado-final/resultado-final.component';
import { ResultadoContaReceberComponent } from './resultado-conta-receber/resultado-conta-receber.component';
import { ResultadoContaPagarComponent } from './resultado-conta-pagar/resultado-conta-pagar.component';

const ROUTES: Routes = [
    {
        path: 'resultado-conta-pagar',
        component: ResultadoContaPagarComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'resultado-conta-receber',
        component: ResultadoContaReceberComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'resultado-final',
        component: ResultadoFinalComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class ResultadoRoutingModule { }
